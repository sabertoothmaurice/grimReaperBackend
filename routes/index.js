var express = require('express');
var mobile = require('./mobile');
var login = require('./login');
var admin = require('./admin');
var data = require('./data');
var sms = require('./sms');
var version = "v1";

var allRoutes = function (app) {
    app.use('/', express.Router().get('/', function (req, res) {
        res.send({status : "Grim reaper is harvesting perfectly...!"});
    }));

//    app.use('/mobile', mobile);
    app.use('/login', login);
    app.use('/admin', admin);
    app.use('/data', data);
    app.use('/sms', sms);
};


module.exports = allRoutes;