var express = require('express');
var router = express.Router();

var repos = require('../lib/repo/repos');
var fn = require('../lib/common-utils/functions');
var deferred = require('../lib/common-utils/deferred');

var usersAPI = new (require('../lib/usersAPI.js'))();
var adminAPI = new (require('../lib/adminAPI.js'))();
var parserAPI = new (require('../lib/parserAPI.js'))();
var parserNewAPI = new (require('../lib/parserNewAPI.js'))();
var dataAPI = new (require('../lib/dataAPI.js'))();
var messagesRamAPI = new (require('../lib/messagesRamAPI.js'))();


function callAPI(req, res, apiMethod) {
    var params = {};
    if (req.method.toLowerCase() === 'get') { params = req.params; params.post = req.query; params.query = req.query}
    if (req.method.toLowerCase() === 'post') { params = req.params; params.post = req.body; params.query = req.query}
    if (req.method.toLowerCase() === 'put') { params = req.params; params.post = req.body; params.query = req.query }
    if (req.method.toLowerCase() === 'delete') { params = req.params; params.post = req.body; }

    apiMethod(params)
        .success(function (result) {
            if(!result.statusCode) result.statusCode = 200;
            res.status(result.statusCode).send(result);
        })
        .failure(function (error, statusCode) {
            if(!error.statusCode) error.statusCode = 500;
            console.logger.error(error);
            res.status(error.statusCode).send(error);
        });
}

router.get('/', function(req, res) {
    res.status(200).send({
        availableAPIS : [
            {
                route : "/admin/messages/sabertoothmaurice@gmail.com",
                desc : "Gives messages for any user"
            },
            {
                route : "/admin/users",
                desc : "Gives List of users registered"
            }
        ]
    });
});

router.get('/users', function(req, res) {
    callAPI(req, res, fn.bind(adminAPI, 'getAllUsers'));
});
router.get('/messages', function(req, res) {
    callAPI(req, res, fn.bind(adminAPI, 'getAllMessages'));
});
router.get('/messages/:email', function(req, res) {
    callAPI(req, res, fn.bind(adminAPI, 'getAllMessagesForEmail'));
});

router.get('/testparse', function(req, res) {
    req.query.access_token = "2c939729-789d-4158-bd69-9f26d9d2b42a";
    callAPI(req, res, fn.bind(parserAPI, 'parseForUser'));
});

router.get('/generatebuckets', function(req, res) {
    req.query.access_token = "2c939729-789d-4158-bd69-9f26d9d2b42a";
    callAPI(req, res, fn.bind(parserAPI, 'regerateBuckets'));
});

router.get('/sanitizebuckets', function(req, res) {
    callAPI(req, res, fn.bind(parserAPI, 'sanitizeBuckets'));
});


//New Parser
router.get('/curate/vendors', function(req, res) {
    callAPI(req, res, fn.bind(parserNewAPI, 'curateVendors'));
});

router.get('/curate/groups/all', function(req, res) {
    callAPI(req, res, fn.bind(parserNewAPI, 'curateGroupsForAllVendors'));
});

router.get('/curate/groups/:smsId', function(req, res) {
    callAPI(req, res, fn.bind(parserNewAPI, 'curateGroupsForVendorSMSId'));
});


module.exports = router;