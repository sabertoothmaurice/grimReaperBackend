var express = require('express');
var router = express.Router();

var repos = require('../lib/repo/repos');
var fn = require('../lib/common-utils/functions');
var deferred = require('../lib/common-utils/deferred');

var usersAPI = new (require('../lib/usersAPI.js'))();
var dataAPI = new (require('../lib/dataAPI.js'))();
var messagesRamAPI = new (require('../lib/messagesRamAPI.js'))();


function callAPI(req, res, apiMethod) {
    var params = {};
    if (req.method.toLowerCase() === 'get') { params = req.params; params.post = req.query; params.query = req.query}
    if (req.method.toLowerCase() === 'post') { params = req.params; params.post = req.body; params.query = req.query}
    if (req.method.toLowerCase() === 'put') { params = req.params; params.post = req.body; params.query = req.query }
    if (req.method.toLowerCase() === 'delete') { params = req.params; params.post = req.body; }

    apiMethod(params)
        .success(function (result) {
            if(!result.statusCode) result.statusCode = 200;
            res.status(result.statusCode).send(result);
        })
        .failure(function (error, statusCode) {
            if(!error.statusCode) error.statusCode = 500;
            console.logger.error(error);
            res.status(error.statusCode).send(error);
        });
}


router.post('/dump', function(req, res) {
    console.log('REACH 1 : '+JSON.stringify(req.query));
    callAPI(req, res, fn.bind(dataAPI, 'updateSMSDump'));
});



module.exports = router;