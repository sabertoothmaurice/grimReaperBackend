var express = require('express');
var router = express.Router();

var repos = require('../lib/repo/repos');
var fn = require('../lib/common-utils/functions');
var deferred = require('../lib/common-utils/deferred');

var usersAPI = new (require('../lib/usersAPI.js'))();
var dataAPI = new (require('../lib/dataAPI.js'))();
var messagesRamAPI = new (require('../lib/messagesRamAPI.js'))();


function callAPI(req, res, apiMethod) {
    var params = {};
    if (req.method.toLowerCase() === 'get') { params = req.params; params.post = req.query; }
    if (req.method.toLowerCase() === 'post') { params = req.params; params.post = req.body;params.query = req.query; }
    if (req.method.toLowerCase() === 'put') { params = req.params; params.post = req.body; }
    if (req.method.toLowerCase() === 'delete') { params = req.params; params.post = req.body; }

    apiMethod(params)
        .success(function (result) {
            if(!result.statusCode) result.statusCode = 200;
            res.status(result.statusCode).send(result);
        })
        .failure(function (error, statusCode) {
            if(!error.statusCode) error.statusCode = 500;
            console.logger.error(error);
            res.status(error.statusCode).send(error);
        });
}




//Data routes
router.get('/transactions', function(req, res) {
    callAPI(req, res, fn.bind(dataAPI, 'getTransactions'));
});
router.get('/mycards', function(req, res) {
    callAPI(req, res, fn.bind(dataAPI, 'getCardsForUser'));
});
router.get('/allvendors', function(req, res) {
    callAPI(req, res, fn.bind(dataAPI, 'getAllVendors'));
});
router.get('/allregexs', function(req, res) {
    callAPI(req, res, fn.bind(dataAPI, 'getAllRegex'));
});
router.post('/createregex', function(req, res) {
    callAPI(req, res, fn.bind(dataAPI, 'createRegexNew'));
});
router.get('/editbucket', function(req, res) {
    callAPI(req, res, fn.bind(dataAPI, 'getEditBucketPreData'));
});
router.post('/editbucket', function(req, res) {
    callAPI(req, res, fn.bind(dataAPI, 'editBucket'));
});
router.get('/deleteregex', function(req, res) {
    callAPI(req, res, fn.bind(dataAPI, 'deleteRegex'));
});



module.exports = router;