'use strict';
var deferred = require('./common-utils/deferred');
var fn = require('./common-utils/functions');
var repos = require('./repo/repos.js');
var _ = require('underscore');
var uuid = require('node-uuid');
var activeThreads = [];
activeThreads.push({
    threadId : 'Global',
    name    :   'Global Group',
    description : 'A room with all TinyStep Users'
});
activeThreads.push({
    threadId : 'Local',
    name    :   'Local Group',
    description : 'A room with some TinyStep Users'
});

function MessagesAPI() {}

function apiRespose(success,result){//use this to reply to all API requests
    if(success){
        return deferred.success({
            status		:	'success',
            message	    :	null,
            result		:	result
        });
    }else{
        return deferred.success({
            status		:	'error',
            message	    :	result,
            result		:	null
        });
    }
}

//CRUD
MessagesAPI.prototype.readMessages = function (params) {
    var msgs = [];
//    msgs.push({
//        message: "bababa askdhg dhjksg jdfhsk jghdk g",
//        from: {
//            userId: "22",
//            username: "caca",
//            picUrl: null
//        },
//        threadId: "Global",
//        id: 23,
//        timestamp: 1436188687726000
//    });
//    msgs.push({
//        message: "bababa askdhg dhjksg jdfhsk jghdk g",
//        from: {
//            userId: "22",
//            username: "caca",
//            picUrl: null
//        },
//        threadId: "Global",
//        id: 23,
//        timestamp: 1436188687729000
//    });
//    return apiRespose(true,{msgs:msgs});

    var userId = params.userId;
    var prevMsgTime = params.prevMsgTime;
    var msgsCall = fn.defer(fn.bind(repos.messagesRepo, 'getAllMessagesBeforeD'))({prevMsgTime:prevMsgTime});
    var allUsersCall = fn.defer(fn.bind(repos.usersRepo, 'getAllUsersD'))({userId:userId});
    return deferred.combine({msgsCall:msgsCall,allUsersCall:allUsersCall}).pipe(function(data) {
        var usersData = _.indexBy(data.allUsersCall,'userId');
//        if(usersData[userId]==null) return apiRespose(false,"User is not registered");
        var msgs = data.msgsCall;
        for(var index in msgs){
            var msg = msgs[index];
            msg.from = usersData[msg.from];
        }

        fn.defer(fn.bind(repos.usersRepo, 'updateLastPingTimeD'))({
            userId      :   userId,
            lastPingTime    :   (new Date()).getTime()
        }).pipe(function(data) {
            return deferred.success({});
        });
        console.log("Sending "+msgs.length+" unsent messages");
        return apiRespose(true,{msgs:msgs});
    });
};
MessagesAPI.prototype.addMessage = function (params) {
    console.log('params',params);
    var userId = params.userId;
    var message = params.post.message;
    var threadId = params.post.threadId;
    return fn.defer(fn.bind(repos.usersRepo, 'getUserD'))({userId:userId}).pipe(function(userData){
        if(userData==null) return apiRespose(false,"User is not registered");//TODO: register user if he is not yet registered
        return fn.defer(fn.bind(repos.messagesRepo, 'addMessageD'))({
            message     :   message,
            from        :   userId,
            threadId    :   threadId
        }).pipe(function(data) {
            return apiRespose(true,{data:data});
        });
    });
};
MessagesAPI.prototype.registerAndGetIrcGroups = function (params) {
    //TODO:complete this function
    console.log('params registerAndGetIrcGroups',params);
    var userId = params.userId;
    var username = params.post.username;
    var picUrl = params.post.picUrl;
    return fn.defer(fn.bind(repos.usersRepo, 'getUserD'))({
        userId     :   userId
    }).pipe(function(existingUser){
        if(existingUser){
            return fn.defer(fn.bind(repos.usersRepo, 'updateUserD'))({
                userId      :   userId,
                username    :   username,
                picUrl      :   picUrl
            }).pipe(function(data) {
                console.log("This user already exists, hence updating",{
                    userId      :   userId,
                    username    :   username,
                    picUrl      :   picUrl
                });
                return apiRespose(true,{ircRooms : activeThreads});
            });
        }else{
            return fn.defer(fn.bind(repos.usersRepo, 'addUserD'))({
                userId      :   userId,
                username    :   username,
                picUrl      :   picUrl
            }).pipe(function(data) {
                console.log("This user is a new user, hence creating",{
                    userId      :   userId,
                    username    :   username,
                    picUrl      :   picUrl
                });
                return apiRespose(true,{ircRooms : activeThreads});
            });
        }
    });

};

module.exports = MessagesAPI;
