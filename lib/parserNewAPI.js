'use strict';
var deferred = require('./common-utils/deferred');
var fn = require('./common-utils/functions');
var repos = require('./repo/repos.js');
var _ = require('underscore');
var uuid = require('node-uuid');
var regexp = require('node-regexp');
var constants = new (require("./common-utils/constants.js"))();
var moment = require('moment');
var uniqueCode = "RATSHD";//used as placeholder while generating parser
function generateUniqueCode(){
    var text = "";
    var possible = "0123456789";
    for( var i=0; i < 6; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function ParserNewAPI() {}

function apiRespose(success,result,status){//use this to reply to all API requests
    if(!status) status = 200;
    if(success){
        return deferred.success({
            status		:	'success',
            message	    :	null,
            result		:	result,
            statusCode  :   status
        });
    }else{
        return deferred.success({
            status		:	'error',
            message	    :	result,
            statusCode  :   status
        },status);
    }
}


/**
 * This searches for all the possible vendors in our space
 */
ParserNewAPI.prototype.curateVendors = function (params) {
    console.log("Started curateVendors");
    var deferredCallsL1 = {};
    deferredCallsL1.senders = fn.defer(fn.bind(repos.smsdumpRepo, 'getSendersD'))({});
    deferredCallsL1.vendorsExistingIds = fn.defer(fn.bind(repos.vendorsRepo, 'getAllVendorsSMSIdsD'))({});
    return deferred.combine(deferredCallsL1).pipe(function(dataL1) {
        var senders = dataL1.senders;
        var vendorsExistingIds = dataL1.vendorsExistingIds;


        console.log("Found "+vendorsExistingIds.length+" smsIds");

        //Find Vendors and numbers
        var vendorIds = [];
        var numberIds = [];
        senders.forEach(function(sender){
            var id = sender._id;
            if(isVendor(id)){
                var actualVendorId = id.split("-")[1];
                if(vendorIds.indexOf(actualVendorId)<0) vendorIds.push(actualVendorId);
            }
        });
        console.log("found "+vendorIds.length+" unique senderIds from SMSRAW");

        //Find new vendors
        var vendorsTogenerate = [];
        vendorIds.forEach(function(vendor){
            if(vendorsExistingIds.indexOf(vendor)<0) vendorsTogenerate.push(vendor)
        });

        console.log("Found "+vendorsTogenerate.length+" to generate");

        //Generate new Vendors
        if(vendorsTogenerate.length==0) return apiRespose(true,{vendorsExistingIds:vendorsExistingIds,vendorsTogenerate : vendorsTogenerate});
        var deferredCallsL2 = {};
        var count = 0;
        vendorsTogenerate.forEach(function(vendorNewId){
            deferredCallsL2[count++] = fn.defer(fn.bind(repos.vendorsRepo, 'createVendorD'))({
                name : "Unknown Name",
                smsId : vendorNewId,
                image : null
            });
        });
        return deferred.combine(deferredCallsL2).pipe(function(result){
            console.log("Generated "+vendorsTogenerate.length+" new vendors");
            return apiRespose(true,{vendorsExistingIds:vendorsExistingIds,vendorsTogenerate : vendorsTogenerate},200);
        });
    });
};

function isVendor(sender){
    return (sender.indexOf("-")>=0);
}

/**
 *  THIS IS STEP 2
 *
 *  Generates buckets from smsDump and stores them in preBucketsRepo
 *
 */
ParserNewAPI.prototype.curateGroupsForVendorSMSId = function (params) {
    var smsId = params.smsId || params.post.smsId;
    return curateGroupsForVendorSMSIdD(smsId).pipe(function(data){
        return apiRespose(true, data, 200);
    });
};

function curateGroupsForVendorSMSIdD(smsId){
    var start = (new Date()).getTime();

    console.log("Started curateGroupsForVendorSMSId : "+smsId);
    var deferredCallsL1 = {};
    deferredCallsL1.smsRawDump = fn.defer(fn.bind(repos.smsdumpRepo, 'getAllMessagesForSMSIdD'))({smsId:smsId});
    return deferred.combine(deferredCallsL1).pipe(function (dataL1) {
        var smsRawDump = dataL1.smsRawDump;


        //Group similar messages into groups
        start = (new Date()).getTime();
        var orderedBuckets = orderIntoBuckets(smsRawDump);
        var groups = [];
        orderedBuckets.forEach(function(bucket){
            var group = {sample : null, extras : []};
            bucket.msgs.forEach(function(sms){
                if(!group.sample) group.sample = sms.body;
                else{
                    group.extras.push(sms.body);
                }
            });
            groups.push(group);
        });
        console.log("Grouped similar messages in "+((new Date()).getTime()-start)+" ms");

        //Filter only valid Regexs(ones with more than 4 extras)
        start = (new Date()).getTime();
        var validGroups = [];
        var MIN_REGEX_GROUP_NUM = 2;
        groups.forEach(function(group) {
            if (group.extras.length > MIN_REGEX_GROUP_NUM-1 ) validGroups.push(group);
        });
        console.log("Filtered into "+groups.length+" groups in "+((new Date()).getTime()-start)+" ms");

        //Find common regex
        start = (new Date()).getTime();
        var invalidGroups = [];
        validGroups.forEach(function(group){
            group.regex = getCommonRegexForGroup(group.sample,group.extras);
            invalidGroups.push(group);
        });

        //remove empty regex groups
        var tempGroups = validGroups;
        validGroups = [];
        tempGroups.forEach(function(group){
            if((new RegExp(group.regex,"g")).exec(group.sample)) validGroups.push(group);
        });
        console.log("Found common regex in "+((new Date()).getTime()-start)+" ms");

        //Find number of messages sorted through regexes generated
        start = (new Date()).getTime();
        var sorted = 0;
        var unsorted = [];
        smsRawDump.forEach(function(sms){
            var done = false;
            for(var i=0;i<validGroups.length;i++){
                var regex = new RegExp(validGroups[i].regex);
                var regexMatch = regex.exec(sms.body);
                if(regexMatch!=null){
                    sorted++;
                    done = true;
                    break;
                }
            }
            if(!done) unsorted.push(sms.body);
        });
        console.log("Finding analytics on messages successfully sorted in "+((new Date()).getTime()-start)+" ms");

        //Smart Update existing Regex Repo
//        return removeredundantBuckets(smsId).pipe(function(upsertData) {
//            return deferred.success({
//                groupsValid: validGroups,
//                sorted: sorted + "/" + smsRawDump.length,
//                unsorted: unsorted,
//                upsertData:upsertData
//            });
//        });
        return upsertBucketsInDb(validGroups,smsId).pipe(function(upsertData){
            //Remove redundant buckets
            console.log("Successfullt unserted");
            return removeredundantBuckets(smsId).pipe(function(redundantData) {
                return deferred.success({
                    groupsValid:validGroups,
                    sorted :sorted+ "/"+smsRawDump.length,
                    unsorted:unsorted,
                    upsert : upsertData
                });
            });

        });
    });
}

function removeredundantBuckets(smsId){
    console.log("Started removing redundant Buckets");
    var deferredCallsL1 = {};
    deferredCallsL1.allPreBuckets = fn.defer(fn.bind(repos.prebucketRepo, 'getPreBucketsForSMSId'))({smsId : smsId});
    return deferred.combine(deferredCallsL1).pipe(function (dataL1) {
        var allPreBuckets = dataL1.allPreBuckets;
        var buckets = [];
        allPreBuckets.forEach(function(preBucket){

            var randomSample = preBucket.regex;
            while(randomSample.indexOf("(.+)")!=-1){
                randomSample = randomSample.replace("(.+)",generateUniqueCode());
            }

            buckets.push({
                id : preBucket._id,
                regex : preBucket.regex,
                sample : randomSample
            });
        });


        //Find children
        var finalBuckets = _.pluck(buckets,"id");
        buckets.forEach(function(bucket1){
            buckets.forEach(function(bucket2){
                if(bucket1.id==bucket2.id) return;
                var regex1 = new RegExp(bucket1.regex);
                var regex2 = new RegExp(bucket2.regex);
                var regexMatch1 = regex1.exec(bucket2.sample);
                var regexMatch2 = regex2.exec(bucket1.sample);
                if(regexMatch1&&regexMatch2){
                    //both are siblings
//                    console.log("RELATION : sibling : "+bucket1.regex+":"+bucket2.regex);

                    if (bucket1.regex==bucket2.regex)//remove if same regex
                        finalBuckets.splice( finalBuckets.indexOf(bucket2.id), 1 );

                    if (finalBuckets.indexOf(bucket1.id)>=0 && finalBuckets.indexOf(bucket2.id)>=0)
                        finalBuckets.splice( finalBuckets.indexOf(bucket2.id), 1 );


                }else if(regexMatch1){
                    //Bucket1 is parent of bucket2
//                    console.log("RELATION : parent1 : ");
//                    console.log("parent1",bucket1.regex);
//                    console.log("parent2",bucket2.regex);

                    if (finalBuckets.indexOf(bucket1.id)>=0 && finalBuckets.indexOf(bucket2.id)>=0)
                        finalBuckets.splice( finalBuckets.indexOf(bucket2.id), 1 );

                }else if(regexMatch2){
                    //Bucket2 is parent of bucket1
//                    console.log("RELATION : parent2 : ");
//                    console.log("parent1",bucket1.regex);
//                    console.log("parent2",bucket2.regex);

                    if (finalBuckets.indexOf(bucket1.id)>=0 && finalBuckets.indexOf(bucket2.id)>=0)
                        finalBuckets.splice( finalBuckets.indexOf(bucket1.id), 1 );


                }else{
                    //No relation
                }
            });
        });

        var finalBucketsD = [];
        var bucketsMap = _.indexBy(buckets,"id");
        finalBuckets.forEach(function(id){
            finalBucketsD.push(bucketsMap[id]);
        });

        //See whom we have to remove
        var bucketsToRemove = [];
        buckets.forEach(function(bucket){
            if(finalBuckets.indexOf(bucket.id)<0)
                bucketsToRemove.push(bucket.id);
        });


        console.log("bucketsToRemove",bucketsToRemove);
        var deferredCallsL2 = {};
        deferredCallsL2.allPreBuckets = fn.defer(fn.bind(repos.prebucketRepo, 'removePreBucketsForIdsD'))({ids :  bucketsToRemove});
        return deferred.combine(deferredCallsL2).pipe(function (dataL2) {
            console.log("Successfully removed "+bucketsToRemove.length+" redundant buckets");
            return deferred.success({buckets:buckets,removeRedundantDeferred:dataL2})
        });


    });
}

function upsertBucketsInDb(buckets,smsId){
    if(buckets.length==0) return deferred.success({});
    console.log("Started Updating Buckets in DB");
    var deferredCallsL1 = {};
    deferredCallsL1.allPreBuckets = fn.defer(fn.bind(repos.prebucketRepo, 'getPreBucketsForSMSId'))({smsId : smsId});
    return deferred.combine(deferredCallsL1).pipe(function (dataL1) {
        var allPreBuckets = dataL1.allPreBuckets || [];
        console.log("Found "+allPreBuckets.length+" existing buckets for this vendor")
        var allPreBucketsIds = _.pluck(allPreBuckets, '_id');
        var deferredCallsL2 = {};
        var count = 0;

        buckets.forEach(function(bucket){

            var regex = new RegExp(bucket.regex);
            var foundExistingMatch = false;
            allPreBuckets.forEach(function(existingBucket){
                var regexMatch  = regex.exec(existingBucket.sample);
                if(regexMatch!=null){
                    //Found existing bucket that matches
                    foundExistingMatch = true;
                    console.log("Updating existing Bucket : "+existingBucket._id);
                    var index = allPreBucketsIds.indexOf(existingBucket._id);
                    if (index >= 0) allPreBucketsIds.splice( index, 1 );
                    deferredCallsL2[count++] = fn.defer(fn.bind(repos.prebucketRepo, 'upsertBucketById'))({
                        id          :   existingBucket._id,
                        regex       :   bucket.regex,
                        variables   :   findVariablesTypes(bucket),
                        category    :   findCategoryOfBucket(bucket)
                    });
                }
            });

            //Did not found existing Bucket that Matched
            if(!foundExistingMatch){
                console.log("Created a new Bucket..!");
                deferredCallsL2[count++] = fn.defer(fn.bind(repos.prebucketRepo, 'createPreBucketD'))({
                    smsId       :   smsId,
                    sample      :   bucket.sample,
                    extras      :   bucket.extras.slice(0,30),//store only 30 examples
                    regex       :   bucket.regex,
                    variables   :   findVariablesTypes(bucket),
                    category    :   findCategoryOfBucket(bucket)
                });
            }
        });

        //Remove Buckets that are not touched
        console.log("removing Redundant Buckets for Ids ",allPreBucketsIds);
//        if(allPreBucketsIds.length>0){
//            deferredCallsL2.REDUNDANT = fn.defer(fn.bind(repos.prebucketRepo, 'removePreBucketsForIdsD'))({
//                ids : allPreBucketsIds
//            });
//        }
        console.log("deferredCallsL2",deferredCallsL2);
        return deferred.combine(deferredCallsL2).pipe(function(data){
            return deferred.success({
                upserDeferred : data
            });
        });

    });
}

function findCategoryOfBucket(bucket){
    var categories = [];
    var variablesCount = bucket.regex.split("(.+)").length-1;
    if(variablesCount==0) categories.push(constants.bucket_cat_spam);
    if(categories.indexOf(constants.bucket_cat_spam)<0){
        if(bucket.sample.match(new RegExp("(otp|pass)","gi"))) categories.push(constants.bucket_cat_otp);
        if(bucket.sample.match(new RegExp("(transaction|(Rs.(\\s*)(\\d)+))","gi"))) categories.push(constants.bucket_cat_transaction);
    }
    return categories;
}
function findVariablesTypes(bucket){
    var variables = [];
    console.log("bucket",bucket);
    var samplevariables = (new RegExp(bucket.regex,"g")).exec(bucket.sample).splice(1);
//    console.log("samplevariables",samplevariables);
    for(var i=0;i<samplevariables.length;i++){
        var message = bucket.sample+"";
        message = message.replace(samplevariables[i],uniqueCode);

        var variableType = constants.variable_code_unknown;

        //Test for cost
//        console.log("TESTDD",message+" : "+new RegExp("Rs(\\.*)(\\s*)"+uniqueCode+"","gi"));

        if(moment(samplevariables[i]).toString()!="Invalid date") variableType = constants.variable_code_date;
        if(samplevariables.length==1&&message.match(new RegExp("(otp|password)","gi"))) variableType = constants.variable_code_otp;
        if(message.match(new RegExp("Rs(\\.*)(\\s*)"+uniqueCode+"","gi"))) variableType = constants.variable_code_cost;
        if(message.match(new RegExp("(Dear|Hi)(\\s*)"+uniqueCode+"","gi"))) variableType = constants.variable_code_username;
        if(message.match(new RegExp("call(\\.*)(\\s*)"+uniqueCode+"","gi"))) variableType = constants.variable_code_phone;
        if(samplevariables[i].match(new RegExp("080\\d{8}","gi"))) variableType = constants.variable_code_phone;
        if(samplevariables[i].match(new RegExp("\\+91\\d{10}","gi"))) variableType = constants.variable_code_phone;

        //TODO : add date parser for syntax : 19/09/2015 06:50:11


        variables.push(variableType);
    }
    return variables;
}



ParserNewAPI.prototype.curateGroupsForAllVendors= function (params) {

    console.log("Started curateGroupsForAllVendors");
    var deferredCallsL1 = {};
    deferredCallsL1.vendorsExistingIds = fn.defer(fn.bind(repos.vendorsRepo, 'getAllVendorsSMSIdsD'))({});
    return deferred.combine(deferredCallsL1).pipe(function (dataL1) {
//        console.log("dataL1.vendorsExistingIds;",dataL1.vendorsExistingIds);
//        dataL1.vendorsExistingIds = ["SWIGGY","SBIINB"];//testing
        var vendorsExistingIds = dataL1.vendorsExistingIds;
        var done = [];
        var pending = dataL1.vendorsExistingIds;


        function callbackAllDone(){
            console.log("ALL VENDORS UPDATED");
        }

        function tryPendingSMSId(){
            if(done.length==vendorsExistingIds.length){
                callbackAllDone();
            }else{
                var smsId = pending[0];
                console.log("Trying to generate Buckets for smsID : "+smsId);
                curateGroupsForVendorSMSIdD(smsId).success(function(data){
                    var index = pending.indexOf(smsId);
                    if (index >= 0) {
                        pending.splice( index, 1 );
                    }
                    console.log("SUccessfully generated Buckets for : "+smsId);
                    tryPendingSMSId();
                }).failure(function(err){
                    console.logger.error("ERROR",err);
                });
            }
        }

        tryPendingSMSId();//start the loop
//        vendorsExistingIds.forEach(function(vendorSMSId){
//
//        });
        return apiRespose(true, {}, 200);
    });
};
function orderIntoBuckets(msgsArr){
    var count = 0;
    console.log("Started orderIntoBuckets form "+msgsArr.length+" messages");
    var buckets = [];
    msgsArr.forEach(function(sms){
        var found = false;
//        console.log("Checking sms "+(count++)+" among "+buckets.length+" buckets : "+sms.body);

        buckets.forEach(function(bucket){
//            console.log("REACH1 : "+bucket.sample.body);
            if(!found&&isMessagesSimilar(bucket.sample.body,sms.body)){
//                console.log("FOUNd Match : "+bucket.sample.body+" : "+sms.body);
//                if(bucket.msgs.length<5)
                bucket.msgs.push(sms);
                if(!bucket.test) bucket.test = [];
                bucket.test.push(generateCommonRegex(bucket.sample.body,sms.body));
                bucket.length++;
                found = true;
            }
        });

        if(!found){
            buckets.push({
                sample : sms,
                msgs : [sms],
                length : 1
            });

            //Get only 50 top buckets
            if(buckets.length>200){
                console.log("Trimming to only "+buckets.length+" buckets");
                buckets.sort(function(b1, b2){return b2.length-b1.length});
                buckets.splice(-1);
            }
        }
    });
    console.log("finished orderIntoBuckets into: "+buckets.length);
    return buckets;
}


function getChangingVariables(sample,extras){
    //TODO : make it more smarted than this
    var sampleArr = sample.split(" ");//all words in sample
    var uniqueWords = [];
    sampleArr.forEach(function(sampleWord){
        var passed = false;
        extras.forEach(function(extra) {
            if (!passed&&extra.indexOf(sampleWord) < 0) passed = true;
        });
        if(passed) uniqueWords.push(sampleWord);
    });

    return clubRepeatedDiffs(uniqueWords,sample);
}
//function clubRepeatedDiffs(words,sample){
//    var finalWords = [];
//    var stack = [];
//    words.forEach(function(word){
//        if(sample.split(" ").join("").indexOf(stack.join("")+word)>=0){
//            stack.push(word);
//        }else{
//            var stackEndWord = stack[stack.length-1];
//            finalWords.push(sample.slice(sample.indexOf(stack[0]),sample.indexOf(stackEndWord)+stackEndWord.length));
//            stack = [];
//        }
//
//    });
//    var stackEndWord = stack[stack.length-1];
//    if(stackEndWord)
//        finalWords.push(sample.slice(sample.indexOf(stack[0]),sample.indexOf(stackEndWord)+stackEndWord.length));
//
//    return finalWords;
//}

function isMessagesSimilar(str1, str2){
//    console.log("Checking similarity : "+str1+" : "+str2);
    var str1Arr = str1.split(" ");
    var str2Arr = str2.split(" ");
    if(str1Arr.length<5||str2Arr.length<5) return false;
    var minMatchPercent = 0.07;
    var match1 = 0, unmatch1 = 0,match2 = 0, unmatch2 = 0;
    var str1ArrCopy = ""+str1Arr;
    var str2ArrCopy = ""+str2Arr;
    var str1Final = [];
    var str2Final = [];
    str1Arr.forEach(function(str){
        if(str2ArrCopy.indexOf(str)>=0){
            match1++;
            str1Final.push(str);
            str2ArrCopy = str2ArrCopy.replace(str,uniqueCode);//to eliminate from furthur matches
        }
        else {
            unmatch1++;
            str1Final.push(uniqueCode);
        }
    });
    str2Arr.forEach(function(str){
        if(str1ArrCopy.indexOf(str)>=0){
            match2++;
            str2Final.push(str);
            str1ArrCopy = str1ArrCopy.replace(str,uniqueCode);//to eliminate from furthur matches
        }
        else {
            unmatch2++;
            str2Final.push(uniqueCode);
        }
    });

    //See if indexes of replaced
    str1Final = str1Final.join(" ");
    str2Final = str2Final.join(" ");
    str1Final = str1Final.replace(new RegExp("("+uniqueCode+"\\s+)+"+uniqueCode+"","g"), uniqueCode);
    str2Final = str2Final.replace(new RegExp("("+uniqueCode+"\\s+)+"+uniqueCode+"","g"), uniqueCode);
    if(str1Final!=str2Final) return false;

    return ((unmatch1 / (match1 + unmatch1))*(unmatch2 / (match2 + unmatch2))) <= minMatchPercent;
}

function getCommonRegexForGroup(sample,extras){
    var arrSplit = [" ",",",".","(",")","\\",":","\r"];
    sample = sample.replace("\r","");//To be tested, added to remove bug related to airtel roaming messages
//    sample = sample.replace("\n","");
    var sampleNew = ""+sample;
    arrSplit.forEach(function(token){sampleNew = sampleNew.split(token).join(" ")});
    var sampleNewArr = sampleNew.split(" ");
    var sampleNewArr2 = [];
    var index = 0;
    sampleNewArr.forEach(function(word){
        sampleNewArr2.push({
            word : word,
            index : index,
            boundary : sample.substr((index!=0)?index-1:0,word.length+2)
        });
        index += word.length+1;//add space
    });
    var sampleRegex = ""+sample;
    sampleNewArr2.forEach(function(variable){
        if(variable.word=="") return;

        var variableWithBoundaries = variable.boundary;
        for(var i=0;i<extras.length;i++){
            if(extras[i].indexOf(variableWithBoundaries)<0){
                sampleRegex = sampleRegex.replace(variable.word,uniqueCode);
                console.log("NOTFOUND : "+variable.word + " : "+sampleRegex);
                break;
            }
        };
    });

    //Remove regex
    sampleRegex = sampleRegex.replace(new RegExp("("+uniqueCode+"(\\s|\\.|:)+)*"+uniqueCode+"(\\.){0,1}","g") , uniqueCode);// abcd % % abcd > abcd % abcd

    sampleRegex = escapeRegExp(sampleRegex);// abcd Rs.% > abcd Rs\.%
    sampleRegex = sampleRegex.replace(new RegExp(uniqueCode,"g"), "(.+)");


    //Check before returning
    var regex = new RegExp(sampleRegex);
    var regexMatch = regex.exec(sample);
    if(regexMatch==null){
        //TODO : solve this......
        console.logger.error("ERROR generating proper REGEX : ",{
            sample : sample,
            extras : extras,
            sampleRegex : sampleRegex
        });
        return null;
    };
    console.log("REGEX GENERATED : ",sampleRegex);

    return sampleRegex;
}

function generateCommonRegex(str1,str2){
//    console.log("generateCommonRegex===========");
    var str1Arr = str1.split(" ");
    var str1Final = [];
    var str2Arr = str2.split(" ");
    var str2Final = [];
    var str1ArrCopy = ""+str1Arr;
    var str2ArrCopy = ""+str2Arr;
    str1Arr.forEach(function(str){
        if(str2ArrCopy.indexOf(str)>=0){
            str1Final.push(str);
            str2ArrCopy = str2ArrCopy.replace(str,uniqueCode);
        }else{
            str1Final.push(uniqueCode);
        }
    });
    str2Arr.forEach(function(str){
        if(str1ArrCopy.indexOf(str)>=0){
            str2Final.push(str);
            str1ArrCopy = str1ArrCopy.replace(str,uniqueCode);
        }else{
            str2Final.push(uniqueCode);
        }
    });
    str1Final = str1Final.join(" ");
    str2Final = str2Final.join(" ");
//    console.log("str1preFinal",str1);
//    console.log("str1[reFinal",str2);
    str1Final = str1Final.replace(new RegExp("("+uniqueCode+"\\s+)+"+uniqueCode+"","g"), uniqueCode);
    str2Final = str2Final.replace(new RegExp("("+uniqueCode+"\\s+)+"+uniqueCode+"","g"), uniqueCode);
//    console.log("str1Final",str1Final);
//    console.log("str2Final",str2Final);
    return str1Final.replace()
}

function findDiffBetweenString(str1,str2,arrSplit){ //finds all strings that are different in str2 that are in str1
    arrSplit = [" ",",",".","(",")","\\",":"];

    var str1New = ""+str1;
    arrSplit.forEach(function(token){str1New = str1New.split(token).join(" ")});
    var str1Arr = str1New.split(" ");

    var str2New = ""+str2;
    arrSplit.forEach(function(token){str2New = str2New.split(token).join(" ")});
    var str2Arr = str2New.split(" ");

    var diff = [];
    str1Arr.forEach(function(str){
        if(str2Arr.indexOf(str)<0) diff.push(str);
    });

//    console.log("diff : ",diff);
//    console.log("str1 : ",str1);
//    console.log("str2 : ",str2);
//    console.log("str1New : ",str1New);

    //club space seperated diffs
    var previousStart = 0;
    var previousEnd = 0;
    var diffFinal = [];
    diff.forEach(function(diffStr){
        var startIndex = str1.indexOf(diffStr);
        var endIndex = startIndex+diffStr.length;
        var betweenText = str1.substr(previousEnd, startIndex-previousEnd);
        arrSplit.forEach(function(token){betweenText = betweenText.split(token).join("")});
//        console.log("betweenText : ",betweenText);
        if(betweenText.length!=0){
//            console.log("Pushing : "+previousStart+" : "+previousEnd,str1.substr(previousStart,previousEnd-previousStart));
            diffFinal.push(str1.substr(previousStart,previousEnd-previousStart));
            previousStart = startIndex;
            previousEnd = endIndex;
        }else{
            previousEnd = endIndex;
        }
    });
    diffFinal.push(str1.substr(previousStart,previousEnd-previousStart));//last one too

    while(diffFinal.indexOf("")>-1){
        var index = diffFinal.indexOf("");
        diffFinal.splice(index, 1);
    }


//    console.log("diffFinal : ",diffFinal);
//    console.log("-------------");

    return diffFinal;
}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}
function unescapeRegExp(str) {
    return str.replace(/\\\-/g,"-").replace(/\\\[/g,"[").replace(/\\\]/g,"]").replace(/\\\//g,"/").replace(/\\\{/g,"{")
        .replace(/\\\}/g,"}").replace(/\\\(/g,"(").replace(/\\\)/g,")").replace(/\\\*/g,"*").replace(/\\\+/g,"+").replace(/\\\?/g,"?")
        .replace(/\\\./g,".").replace(/\\\\/g,"\\").replace(/\\\^/g,"^").replace(/\\\$/g,"$").replace(/\\\|/g,"|");
}
function clubRepeatedDiffs(diffs,body){
    console.log("Clubbing repeated : "+body+"-----------------------");
    var newDiffs = [];
    var wordsFinal = [];
    var arr = {};
    diffs.forEach(function(diff){
        for(var i=diff.startIndex;i<diff.endIndex;i++){
            arr[i] = 1;
        }
    });
    arr[body.length] = null;
    var prev = null;
    var starts = [];
    var ends = [];
    for(var i=0;i<body.length+1;i++){
        if((arr[i]==null||i==0)&&(arr[i+1]!=null)){
            starts.push((i==0)?i:i+1);
        }
        if((arr[i+1]==null)&&(arr[i]!=null)){
            ends.push(i+1);
        }
    }
    for(var i=0;i<starts.length;i++){
        wordsFinal.push(body.substr(starts[i],ends[i]-starts[i]));
    }
//    console.log("diffs",diffs);
//    console.log("newDiffs",newDiffs);
    return wordsFinal;
}

module.exports = ParserNewAPI;
