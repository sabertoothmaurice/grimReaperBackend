'use strict';
var deferred = require('./common-utils/deferred');
var fn = require('./common-utils/functions');
var repos = require('./repo/repos.js');
var _ = require('underscore');
var uuid = require('node-uuid');
var parserAPI = new (require('./parserAPI.js'))();
var constants = new (require("./common-utils/constants.js"))();

//PARAMS
var MAXIMUM_TRANSACTION_TIME = 4* 60 * 60 * 1000;//4hours



function DataAPI() {}

function apiRespose(success,result,status){//use this to reply to all API requests
    if(!status) status = 200;
    if(success){
        return deferred.success({
            status		:	'success',
            message	    :	null,
            result		:	result,
            statusCode  :   status
        });
    }else{
        return deferred.success({
            status		:	'error',
            message	    :	result,
            statusCode  :   status
        },status);
    }
}

//CRUD
DataAPI.prototype.updateSMSDump = function (params) {
    var access_token = params.access_token || params.query.access_token;
    var smsdump = params.post.smsdump;
    return fn.defer(fn.bind(repos.usersRepo, 'getUserByToken'))({
        access_token        :   access_token
    }).pipe(function(user){
        //filter only those points that are far apart
        if(!user) return apiRespose(false,"Invalid token");
        var upsertArr = [];
        smsdump.forEach(function(sms){
            upsertArr.push({
                id : sms.id,
                from : sms.number,
                time : sms.date,
                body : sms.body,
                userId : user._id,
                raw : JSON.stringify(sms),
                uniqueId : sms.id

            });
        });
        var deferredCalls = {};
        upsertArr.forEach(function(msg){
            deferredCalls[msg.uniqueId] = fn.defer(fn.bind(repos.smsdumpRepo, 'updateSMS'))(msg);
        });
        return deferred.combine(deferredCalls).pipe(function(result){
            var inserted = 0;
            var updated = 0;
            for(var key in result){
                if(result[key].upserted) inserted++;
                else updated++;
            }
            console.log(inserted+" messages Inserted, "+updated+" messages updated");

            //Start Parsing the SMS dump as it is updated in async manner
//            parserAPI.regerateBucketsFromUserMsg({userId : user._id}).failure(function(err,res){
//                if(err) console.logger.error("ERROR :",err);
//            });


            return apiRespose(true,{user:user},200);
        });

    });
};

//PROCESSED DATA SERVE CALLS
DataAPI.prototype.getCardsForUser = function (params) {
    console.log("params",params);
    var access_token = params.access_token || params.post.access_token;

    return fn.defer(fn.bind(repos.usersRepo, 'getUserByToken'))({
        access_token        :   access_token
    }).pipe(function(user){

        var deferredCallsL1 = {};
        var self = this;
        deferredCallsL1.smsRaw = fn.defer(fn.bind(repos.smsdumpRepo, 'getAllMessagesForUserD'))({userId :user._id });
        deferredCallsL1.vendors = fn.defer(fn.bind(repos.vendorsRepo, 'getAllVendorsD'))({});
        deferredCallsL1.cards = fn.defer(fn.bind(repos.cardsRepo, 'getAllCardsD'))({});
        return deferred.combine(deferredCallsL1).pipe(function (dataL1) {
            var smsRaw = dataL1.smsRaw;
            var vendors = dataL1.vendors;
            var cards = dataL1.cards;
            var vendorsMap = _.indexBy(vendors,"_id");

            var cardsMatched = [];
            console.log("CARDS",cards);

            cards.forEach(function(card){
                var passedCard = false;
                var cardData = {
                    backup : [],
                    balances :  [],
                    costs   :   [],
                    account : "XXXX"
                };
                var cards = {};

                smsRaw.forEach(function(sms){
                    var passedOneRegex = false;
                    card.regexes.forEach(function(regexMatch){
                        var regex = new RegExp(regexMatch.regex);
                        if(regex.exec(sms.body)) {
                            passedOneRegex = true;
//                            console.log("PASSED : ",sms.body+ " : "+regexMatch.regex);
                            var data  = parseVariables(regexMatch.variables, regex.exec(sms.body).splice(1));
                            if(data!=null){
                                if(data["account"]) cardData.account =  data["account"];
                                var account = data["account"];
                                if(!cards[account]) cards[account] = {
                                    backup : [],
                                    balances :  [],
                                    costs   :   [],
                                    account : account
                                };


                                if(data["cost"]) cards[account].costs.push({
                                    time : sms.time,
                                    cost : parseFloat(data["cost"].split(',').join(""))
                                });
                                if(data["balance"]) cards[account].balances.push({
                                    time : sms.time,
                                    balance : parseFloat(data["balance"].split(',').join(""))
                                });
                                cards[account].backup.push(data);


                                //TODO : about to be deprecated
                                if(data["cost"]) cardData.costs.push({
                                    time : sms.time,
                                    cost : parseFloat(data["cost"].split(',').join(""))
                                });
                                if(data["account"]) cardData.account =  data["account"];
                                if(data["balance"]) cardData.balances.push({
                                    time : sms.time,
                                    balance : parseFloat(data["balance"].split(',').join(""))
                                });
                                cardData.backup.push(data);
                            }
                        }
                    });

                    if(passedOneRegex){
                        passedCard = true;
                    }

                    //Scanning for Card


                });
                if(passedCard){
                    cardData.balances.sort(function(a,b){return a.time- b.time});
                    cardData.costs.sort(function(a,b){return a.time- b.time});
                    var resultCard = {};
                    if(vendorsMap[card.vendorId]){
                        var subCategory = (card.subCategory)?(card.subCategory):"";
                        resultCard.cardName = vendorsMap[card.vendorId].name + " "+subCategory;
                        resultCard.vendorName = vendorsMap[card.vendorId].name;
                        resultCard.logo = vendorsMap[card.vendorId].logo;

                    }
                    resultCard._id = card.vendorId;
                    resultCard.cards = cards;
                    resultCard.cardData = cardData;

                    cardsMatched.push(resultCard);

                }
            });



            return apiRespose(true,{cards:cardsMatched},200);
        });
    });



};
DataAPI.prototype.getTransactions = function (params) {
    var access_token = params.access_token || params.post.access_token;
    return fn.defer(fn.bind(repos.usersRepo, 'getUserByToken'))({
        access_token        :   access_token
    }).pipe(function(user){
        if(!user) return apiRespose(false,"Invalid token");
        var deferredCallsL2 = {};
        var self = this;
        deferredCallsL2.buckets = fn.defer(fn.bind(repos.prebucketRepo, 'getAllTransactionsPreBucketsD'))({});
        deferredCallsL2.smsRaw = fn.defer(fn.bind(repos.smsdumpRepo, 'getAllMessagesForUserD'))({userId :user._id });
        deferredCallsL2.vendors = fn.defer(fn.bind(repos.vendorsRepo, 'getAllVendorsD'))({});
        return deferred.combine(deferredCallsL2).pipe(function (dataL1) {
            var buckets = dataL1.buckets;
            var vendors = dataL1.vendors;
            var smsRaw = dataL1.smsRaw;
//            return deferred.success({buckets:buckets});
            var transactions = getAllTransactions(smsRaw, buckets,vendors);
            transactions.sort(function(t1, t2){return t2.time-t1.time});
            console.log("found " + transactions.length + " transactions of "+smsRaw.length+" messages");
            transactions = eliminateLinkedTransactions(transactions,vendors);
            console.log("compressed to " + transactions.length + " transactions");
            transactions.sort(function(t1, t2){return t2.time-t1.time});
            return apiRespose(true, {transactionsArr: transactions}, 200);
        });
    });
}

function eliminateLinkedTransactions(transactions){
    var resArr = [];
    for(var i=0;i<transactions.length-1;i++){
        var firstOne = transactions[i];
        var secondOne = transactions[i+1];
        if(Math.abs(firstOne.time-secondOne.time)<MAXIMUM_TRANSACTION_TIME){
            if(firstOne.cost==secondOne.cost){
                if(firstOne.vendorCategory!=secondOne.vendorCategory) {
                    //TODO : reove this temp swap if bank message comes after vendro msg
                    if(firstOne.vendorCategory=="bank"){
                        firstOne = transactions[i+1];
                        secondOne = transactions[i];
                    }
                    firstOne.from = {
                        name : secondOne.vendorName
                    };
                    firstOne.source.push(secondOne.source[0]);
                    resArr.push(firstOne);
                    i++;
                    continue;
                }
            }
        }
        resArr.push(transactions[i]);
    }
    return resArr;
}



DataAPI.prototype.getAllVendors = function (params) {
    console.log("params",params);
    var access_token = params.access_token || params.post.access_token;
    return fn.defer(fn.bind(repos.usersRepo, 'getUserByToken'))({
        access_token        :   access_token
    }).pipe(function(user){
        if(!user) return apiRespose(false,"Invalid token");
        return fn.defer(fn.bind(repos.vendorsRepo, 'getAllVendorsD'))({}).pipe(function(vendors){
            var result = [];
            vendors.forEach(function(vendor){
                if(vendor.name!="Unknown Name")
                result.push({
                    vendorId : vendor._id,
                    vendorName : vendor.name,
                    vendorImage : vendor.image
                });
            });
            return apiRespose(true,{vendors:result},200);
        });
    });
};

DataAPI.prototype.getAllRegex = function (params) {
    console.log("params",params);
    var access_token = params.access_token || params.post.access_token;
    return fn.defer(fn.bind(repos.usersRepo, 'getUserByToken'))({
        access_token        :   access_token
    }).pipe(function(user){
        if(!user) return apiRespose(false,"Invalid token");
        var deferredCallsL2 = {};
        var self = this;
        deferredCallsL2.buckets = fn.defer(fn.bind(repos.prebucketRepo, 'getAllPreBucketsD'))({});

        //TODO : this is the major load bearing operation hence removed this temporarily
//        deferredCallsL2.smsRaw = fn.defer(fn.bind(repos.smsdumpRepo, 'getAllMessagesForUserD'))({userId :user._id });
        return deferred.combine(deferredCallsL2).pipe(function (dataL1) {

           var buckets =  dataL1.buckets;

            var result = [];
            buckets.forEach(function(regexDef){
                result.push({
                    _id : regexDef._id,
                    name : regexDef.smsId,
                    category : regexDef.category,
                    samples : regexDef.extras.slice(0,5),
                    variables : regexDef.variables,
                    vendorId : regexDef.smsId,
                    from : regexDef.smsId,
                    regex : regexDef.regex,
                    image : regexDef.image
                });
            });
            return apiRespose(true,{buckets:result},200);
        });
    });
};
DataAPI.prototype.deleteRegex = function (params) {
    console.log("params",params);
    var access_token = params.access_token || params.post.access_token;
    var regexId = params.regexId || params.post.regexId;

    return fn.defer(fn.bind(repos.usersRepo, 'getUserByToken'))({
        access_token        :   access_token
    }).pipe(function(user){
        if(!user) return apiRespose(false,"Invalid token");
        return fn.defer(fn.bind(repos.regexRepo, 'removeRegexById'))({id : regexId}).pipe(function(regexDefs){
//            var result = [];
//            regexDefs.forEach(function(regexDef){
//                result.push({
//                    vendorId : regexDef._id,
//                    vendorName : regexDef.name,
//                    vendorImage : regexDef.image
//                });
//            });
            return apiRespose(true,{regexDefs:regexDefs},200);
        });
    });
};

DataAPI.prototype.createRegexNew = function (params) {
    console.log("createRegexNew params",params);
    var access_token = params.access_token || params.query.access_token;
    var vendorId = params.vendorId || params.post.vendorId;
    var sample = params.sample || params.post.sample;
    var match = params.match || params.post.match;
    var prefix = params.prefix || params.post.prefix;
    var suffix = params.suffix || params.post.suffix;
    var prefixRegex = prefix+"\\s?(\\d*)?";
    var from = params.from || params.post.from;

    return fn.defer(fn.bind(repos.usersRepo, 'getUserByToken'))({
        access_token        :   access_token
    }).pipe(function(user){

        if(!user) return apiRespose(false,"Invalid token");

        return fn.defer(fn.bind(repos.vendorsRepo, 'getvendorById'))({id : vendorId}).pipe(function(vendor){
            if(!vendor) return apiRespose(false,"Invalid Vendor");

            return fn.defer(fn.bind(repos.regexRepo, 'createRegexD'))({
                must : match,
                anythingBut : [],
                regexExtract : [{
                    valueName : "cost",
                    valueRegex: prefixRegex
                }],
                from        : [from],
                vendorId    :   vendorId,
                resultType    :   "Transaction",
                sample    :   sample
            }).pipe(function(regexSaveStatus){
                return apiRespose(true,{regexSaveStatus:regexSaveStatus},200);
            });


        });
    });
};

DataAPI.prototype.getEditBucketPreData = function (params) {
    console.log("params",params);
    var access_token = params.access_token || params.post.access_token;

    return fn.defer(fn.bind(repos.usersRepo, 'getUserByToken'))({
        access_token        :   access_token
    }).pipe(function(user){
        if(!user) return apiRespose(false,"Invalid token");
        return fn.defer(fn.bind(repos.vendorsRepo, 'getAllVendorsD'))({}).pipe(function(vendors){
            return apiRespose(true,{
                categories:constants.categoryTypes,
                vendors:vendors
            },200);
        });
    });
};

DataAPI.prototype.editBucket = function (params) {
    console.log("params",params);
    var access_token = params.access_token || params.query.access_token;
    var bucketId = params._id || params.post._id;
    var bucketname = params.bucketname || params.post.bucketname;
    var variables = params.variables || params.post.variables;
    var category = params.category || params.post.category || constants.categoryDefaultType;
    var vendorId = params.vendorId || params.post.vendorId;
    var extras = params.extras || params.post.extras;



    return fn.defer(fn.bind(repos.usersRepo, 'getUserByToken'))({
        access_token        :   access_token
    }).pipe(function(user){

        if(!user) return apiRespose(false,"Invalid token");

        return fn.defer(fn.bind(repos.bucketRepo, 'updateBucketD'))({
            _id : bucketId,
            name : bucketname,
            variables :  variables,
            category : category,
            vendorId : vendorId,
            extras  :   extras
        }).pipe(function(updateStatus){
            console.log("REACH 2 ",updateStatus);
            return apiRespose(true,{updateStatus:updateStatus},200);
        });
    });
};

function getAllTransactions(smsRawArr,buckets,vendors){
    var vendorsMap = {};
    vendors.forEach(function(vendor){
        vendor.smsId.forEach(function(smsId){
            vendorsMap[smsId] = {
                name : vendor.name,
                image : vendor.image,
                category : vendor.category
            }
        });
    });

    var regexArr = [];
    var transactions = [];
    buckets.forEach(function(bucket){
        regexArr.push(new RegExp(bucket.regex));
    });
    smsRawArr.forEach(function(smsRaw){
        var done = false;
        buckets.forEach(function(bucket){
//            console.log("bucket",bucket);
            if(done) return;
            var regex = new RegExp(bucket.regex);
//            if(!bucket.vendorId) return;
//            console.log("REGEX",regex.exec(smsRaw.body)+" : "+smsRaw.body+" : "+bucket.regex);
            if(regex.exec(smsRaw.body)){
                console.log("Matched a Transaction Bucket");
                done = true;
//                console.log("regex.exec(smsRaw.body)",regex.exec(smsRaw.body).splice(1));
                var data = parseVariables(bucket.variables,regex.exec(smsRaw.body).splice(1));
                data.bucketname = bucket.name;
                if(data.cost==null) return;
                var smsIdForBucket = bucket.smsId;
                var transaction = {
                    cost : parseInt((data.cost.split(",").join(""))), //get the cost parameter
                    data : data,
                    vendorId : bucket.vendorId,
                    smsId : smsIdForBucket,
                    time : smsRaw.time,
                    source : [smsRaw.from +" : "+ smsRaw.body],
                    vendorName : (vendorsMap[smsIdForBucket])?vendorsMap[smsIdForBucket].name:null,
                    vendorImage :  (vendorsMap[smsIdForBucket])?vendorsMap[smsIdForBucket].image:null,
                    vendorCategory :  (vendorsMap[smsIdForBucket])?vendorsMap[smsIdForBucket].category:null
                };
                if(!transaction.cost) transaction.cost = 0;
                transactions.push(transaction);
            }
        });
    });

    return transactions;
}
function parseVariables(arrVariables, allData){
    var data = {};
    var index = 0;
    arrVariables.forEach(function(varname){
        data[varname] = allData[index++];
    });
    return data;
}
function parseVariablesToObj(arrVariables, allData , obj){
    var data = obj;
    var index = 0;
    arrVariables.forEach(function(varname){
        data[varname] = allData[index++];
    });
    return data;
}



module.exports = DataAPI;
