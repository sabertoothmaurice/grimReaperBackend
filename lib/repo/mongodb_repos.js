/**
 * Created by maurice on 18/05/15.
 */

var mongoose = require('mongoose');
var dbConfig = require('config').mongodbConfig;
var underscore = require('underscore');

var dbUrl = 'mongodb://' + dbConfig.host + '/' + dbConfig.database;
var options = {
    user: dbConfig.username,
    pass: dbConfig.password
};
console.log('Connecting to DataBase: ', dbUrl, options);

mongoose.connect('mongodb://' + dbConfig.host + '/' + dbConfig.database,function(err){
    if(err) console.logger.error("MONGO ERROR:",err);
});

mongoose.connection.on('error', function(err) {
    console.logger.error("MONGO ERROR1:",err);
});


//Entity Models
var users_repo = require('./mongo/user_repo')(mongoose);
var vendor_repo = require('./mongo/vendor_repo')(mongoose);
var cards_repo = require('./mongo/cards_repo')(mongoose);
var regex_repo = require('./mongo/regex_repo')(mongoose);
var bucket_repo = require('./mongo/bucket_repo')(mongoose);
var smsdump_repo = require('./mongo/smsraw_repo')(mongoose);
var pre_bucket_repo = require('./mongo/pre_bucket_repo')(mongoose);
var smsprocessed_repo = require('./mongo/smsprocessed_repo')(mongoose);



exports.Users = mongoose.model('Users', users_repo);
exports.Regex = mongoose.model('Regex', regex_repo);
exports.Cards = mongoose.model('Cards', cards_repo);
exports.Buckets = mongoose.model('Bucket', bucket_repo);
exports.Vendors = mongoose.model('Vendors', vendor_repo);
exports.SmsDump = mongoose.model('SmsRawDump', smsdump_repo);
exports.PreBuckets = mongoose.model('PreBucket', pre_bucket_repo);
exports.SmsProcessedDump = mongoose.model('SmsProcessedDump', smsprocessed_repo);
