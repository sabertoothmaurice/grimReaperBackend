var constants = new (require('../../common-utils/constants'))();
module.exports =  function(mongoose){
    //Trains schema
    var PreBucketsSchema = mongoose.Schema({
        smsId       :   String,
        sample      :   String,
        extras      :   [String],
        regex       :   String,
        variables   :   [{ type: String, enum: constants.variable_code, default : constants.variable_code_unknown, required: true}],
        category : [{ type: String, enum: constants.bucket_cat, default: constants.bucket_cat_unknown, required: true}]


    });
    PreBucketsSchema.statics.createPreBucketD = function(data,cb) {
        console.log("Creating PreBucket : "+data.regex);
        this.create({
            smsId       :   data.smsId,
            sample      :   data.sample,
            extras      :   data.extras,
            regex       :   data.regex,
            variables   :   data.variables,
            category    :   data.category || []
        },cb);
    };
    PreBucketsSchema.statics.upsertBucketById = function(data,cb) {
        this.update(
            {_id : data.id},
            {
                regex: data.regex,
                variables: data.variables,
                category: data.category

            },
            {upsert : false}, cb
        );
    };

    PreBucketsSchema.statics.getPreBucketsForSMSId = function(data,cb) {
        this.find({smsId : data.smsId}).sort({_id:1}).exec(cb);
    };

    PreBucketsSchema.statics.getAllPreBucketsD = function(params,cb) {
        this.find({}).lean().exec(cb);
    };
    PreBucketsSchema.statics.getAllTransactionsPreBucketsD = function(params,cb) {
        this.find({category : { $in: [constants.bucket_cat_transaction] }}).lean().exec(cb);
    };

    PreBucketsSchema.statics.removePreBucketsForIdsD = function(params,cb) {
        this.remove({_id : { $in: params.ids }}).exec(cb);
    };

    PreBucketsSchema.statics.getAllVerifiedPreBucketsD = function(params,cb) {
        this.find({verified : true}).lean().exec(cb);
    };

    return PreBucketsSchema;
};
