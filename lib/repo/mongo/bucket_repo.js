var constants = new (require('../../common-utils/constants'))();
module.exports =  function(mongoose){
    //Trains schema
    var BucketsSchema = mongoose.Schema({
        name : String,
        from        : String,
        vendorId    :   String,
        verified    :   {type : Boolean, default : false},

        //result params
        regex  :  { type : String, unique : true},
        variables : [String],
        category : { type: String, enum: constants.categoryTypes, default: constants.categoryDefaultType, required: true},

        //Extra params
        sample      :   [String],
        extras : {}

    });
    BucketsSchema.statics.createBucketD = function(data,cb) {
        console.log("Creating Bucket : "+data.regex);
        this.update({regex: data.regex}
        ,{
            name : data.name,
            from        : data.from,
            vendorId    :   data.vendorId,
            regex : data.regex,
            variables    :   data.variables,
            category    :   constants.categoryDefaultType,
            sample    :   data.sample
        },  {upsert: true},function(err,res){
            if(err) console.logger.error(err);
            cb(err,res);
        });
//        this.update({commentId: data.commentId}, data, {upsert: false}, cb);
    };
    BucketsSchema.statics.updateBucketD = function(data,cb) {
        console.log("updateBucketD",data);
        this.update({_id: data._id}
            ,{
                name : data.name,
                variables    :   data.variables,
                vendorId    :   data.vendorId,
                category    :   data.category || constants.categoryDefaultType,
                extras    :   data.extras
            },  {upsert: false},function(err,res){
                if(err) console.logger.error(err);
                cb(err,res);
            });
//        this.update({commentId: data.commentId}, data, {upsert: false}, cb);
    };
    BucketsSchema.statics.getBucketOfIdD = function(data,cb) {
        this.findOne({id : data.id}, cb);
    };
    BucketsSchema.statics.getAllBucketsD = function(params,cb) {
        this.find({}).lean().exec(cb);
    };
    BucketsSchema.statics.getAllVerifiedBucketsD = function(params,cb) {
        this.find({verified : true}).lean().exec(cb);
    };
    BucketsSchema.statics.getAllTransactionBucketsD = function(params,cb) {
        this.find({category :constants.type_transaction }).lean().exec(cb);
    };
    BucketsSchema.statics.removeBucketById = function(params,cb) {
        this.remove({_id : params.id},cb);
    };
    BucketsSchema.statics.removeBucketByIds = function(params,cb) {
        this.remove({_id : { $in: params.ids }},cb);
    };



    return BucketsSchema;
};
