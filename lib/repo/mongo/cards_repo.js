var constants = new (require('../../common-utils/constants'))();
module.exports =  function(mongoose){
    //Trains schema
    var CardsSchema = mongoose.Schema({
        bank : String,
        cardType : String,
        vendorId    :   String,
        subCategory : String,

        //result params
        regexes : [{
            regex  :  String,
            variables : [String]
        }]
    });
    CardsSchema.statics.createBucketD = function(data,cb) {
        console.log("Creating Bucket : "+data.regex);
        this.update({regex: data.regex}
        ,{
            name : data.name,
            from        : data.from,
            vendorId    :   data.vendorId,
            regex : data.regex,
            variables    :   data.variables,
            category    :   constants.categoryDefaultType,
            sample    :   data.sample
        },  {upsert: true},function(err,res){
            if(err) console.logger.error(err);
            cb(err,res);
        });
//        this.update({commentId: data.commentId}, data, {upsert: false}, cb);
    };
    CardsSchema.statics.updateBucketD = function(data,cb) {
        console.log("updateBucketD",data);
        this.update({_id: data._id}
            ,{
                name : data.name,
                variables    :   data.variables,
                vendorId    :   data.vendorId,
                category    :   data.category || constants.categoryDefaultType,
                extras    :   data.extras
            },  {upsert: false},function(err,res){
                if(err) console.logger.error(err);
                cb(err,res);
            });
//        this.update({commentId: data.commentId}, data, {upsert: false}, cb);
    };
    CardsSchema.statics.getBucketOfIdD = function(data,cb) {
        this.findOne({id : data.id}, cb);
    };
    CardsSchema.statics.getAllCardsD = function(params,cb) {
        this.find({}).lean().exec(cb);
    };
    CardsSchema.statics.getAllTransactionBucketsD = function(params,cb) {
        this.find({category :constants.type_transaction }).lean().exec(cb);
    };
    CardsSchema.statics.removeBucketById = function(params,cb) {
        this.remove({_id : params.id},cb);
    };



    return CardsSchema;
};
