module.exports =  function(mongoose){
    //Trains schema
    var VendorSchema = mongoose.Schema({
        name : String,
        smsId : [String],
        image : String,
        logo : String,
        category : { type: String, enum: ['bank','vendor'], default: 'vendor', required: true}
    });
    VendorSchema.statics.createVendorD = function(data,cb) {
        var smsIds = (data.smsId)?[data.smsId]:[];
        this.create({
            name : data.name,
            smsId : smsIds,
            image : data.image,
            category :   data.category || "vendor"
        }, cb);
    };
    VendorSchema.statics.getvendorById = function(data,cb) {
        this.findOne({_id : data.id}, cb);
    };
    VendorSchema.statics.getAllVendorsD = function(params,cb) {
        this.find(cb);
    };
    VendorSchema.statics.getAllVendorsSMSIdsD = function(params,cb) {
        this.aggregate([
            { $group : { _id : "1", smsIds: { $push: "$smsId" } } }
        ]).exec(function(err,dataL1){
            if(err) return cb(err,null);
            var vendorsExistingIds = [];
            if(dataL1[0]){
                dataL1[0].smsIds.forEach(function(smsIds){
                    smsIds.forEach(function(smsId){
                        vendorsExistingIds.push(smsId);
                    })
                })
            };
            cb(null,vendorsExistingIds);

        });
    };


    VendorSchema.statics.getVendorsForIdsD = function( params, cb ){
        this.find({ _id: { $in: params.ids } }).exec(cb);
    };



    return VendorSchema;
};
