module.exports =  function(mongoose){
    //Trains schema
    var RegexSchema = mongoose.Schema({

        //match params
        regexScan   : {
            must : [String],
            anythingBut : [String]
        },
        regexExtract      :  [new mongoose.Schema({
            valueName : String,
            valueRegex: String
        },{ _id : false })],
        from        : [String],
        vendorId    :   String,

        //result params
        resultType  :  String,

        //Extra params
        sample      :   String

    });
    RegexSchema.statics.createRegexD = function(data,cb) {
        this.create({
            name : data.name,
            image : data.image,
            regexScan : {
                must : data.must || [],
                anythingBut : data.anythingBut || []
            },
            regexExtract : data.regexExtract,
            from        : [data.from],
            vendorId    :   data.vendorId,
            resultType    :   data.resultType,
            sample    :   data.sample
        }, function(err,res){
            if(err) console.logger.error(err);
            cb(err,res);
        });
    };
    RegexSchema.statics.getvendorById = function(data,cb) {
        this.findOne({id : data.id}, cb);
    };
    RegexSchema.statics.getAllRegexesD = function(params,cb) {
        this.find({}).lean().exec(cb);
    };
    RegexSchema.statics.removeRegexById = function(params,cb) {
        this.remove({_id : params.id},cb);
    };



    return RegexSchema;
};
