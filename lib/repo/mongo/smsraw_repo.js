var _ = require('underscore');
module.exports =  function(mongoose){
    //Trains schema
    var SMSRawSchema = mongoose.Schema({
        uniqueId : { type : String, unique : true},
        from : { index  : true, type:  String},
        body : { index  : true, type:  String},
        raw : String,
        time : Number,
        userId : String,


        //Curated Variables
        groupId :   String


    });
    SMSRawSchema.statics.updateSMS = function(data,cb) {
        this.update(
            {userId : data.userId,uniqueId : data.uniqueId},
            {
                from : data.from,
                body : data.body,
                raw : data.raw,
                time : data.time,
                userId : data.userId,
                uniqueId : data.uniqueId
            },
            {upsert : true}, cb
        );
    };
    SMSRawSchema.statics.updateSMSExtraData = function(data,cb) {
        this.update(
            {uniqueId : data.uniqueId},
            {
                groupId : data.groupId
            },
            {upsert : false}, cb
        );
    };
    SMSRawSchema.statics.updateSMSArray = function(data,cb) {
        var ids =_.pluck(data.arr, 'id');
        var self = this;
        this.remove({
            'id' : {
                $in : ids
            }
        }, function(e, docs) {
            self.create(data.arr, cb);//This inserts multiple docs
        });
//        this.update(
//            {userId : data.userId},
//            {
//                from : data.from,
//                body : data.body,
//                raw : data.raw,
//                time : data.time,
//                userId : data.userId
//            },
//            {upsert : true}, cb
//        );
    };
    //ThreadSchema.statics.upsertTrain = function(data,cb) {
    //    this.findOneAndUpdate({trainno : data.trainno, from :data.from, to: data.to}, data, {upsert : true}, cb);
    //};

    SMSRawSchema.statics.getAllMessagesD = function(params,cb) {
        this.find({}).lean().exec(cb);
    };
    SMSRawSchema.statics.getAllMessagesForLimitD = function(params,cb) {
//        this.find(cb).limit(params.limit);
    };

    SMSRawSchema.statics.getRandomMessages = function(params,cb) {
        var self = this;
        var limit = params.limit || 1000;
        this.find({}).count(function(err,count){
            var startLimit = count-limit;
            var skip = Math.round(Math.random()*startLimit);
            self.find({}).skip(skip).limit(limit).exec(cb);
        });

    };

    SMSRawSchema.statics.getAllMessagesForUserD = function(params,cb) {
//        console.log("GGG",params);
        this.find({userId : params.userId}).lean().exec(cb);
    };

    SMSRawSchema.statics.getAllMessagesForSMSIdD = function(params,cb) {
        this.find({from : new RegExp(params.smsId,"")}).limit(params.limit||1000).exec(cb);
    };

    SMSRawSchema.statics.getSendersD = function(params,cb) {
        this.aggregate( [ { $group : { _id : "$from" } } ]).exec(cb);
    };

    return SMSRawSchema;
};
