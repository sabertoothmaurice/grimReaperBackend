var _ = require('underscore');
module.exports =  function(mongoose){
    //Trains schema
    var SMSProcessedSchema = mongoose.Schema({
        uniqueId : { type : String, unique : true},
        from : { index  : true, type:  String},
        body : { index  : true, type:  String},
        time : Number,
        userId : String,
        vendorId : String,
        regexId : String,
        type : { type: String, enum: [
            'Transaction'
        ], default: 'Transaction', required: true, index: true },
        data : {}
    });
    SMSProcessedSchema.statics.updateSMSProcessedD = function(data,cb) {
        this.update(
            {uniqueId : data.uniqueId},
            {
                uniqueId : data.uniqueId,
                from : data.from,
                body : data.body,
                time : data.time,
                userId : data.userId,
                regexId : data.regexId,
                type : data.type,
                vendorId : data.vendorId,
                data : data.data
            },
            {upsert : true}, cb
        );
    };
    SMSProcessedSchema.statics.updateSMSArray = function(data,cb) {
        var ids =_.pluck(data.arr, 'id');
        var self = this;
        this.remove({
            'id' : {
                $in : ids
            }
        }, function(e, docs) {
            self.create(data.arr, cb);//This inserts multiple docs
        });
//        this.update(
//            {userId : data.userId},
//            {
//                from : data.from,
//                body : data.body,
//                raw : data.raw,
//                time : data.time,
//                userId : data.userId
//            },
//            {upsert : true}, cb
//        );
    };
    //ThreadSchema.statics.upsertTrain = function(data,cb) {
    //    this.findOneAndUpdate({trainno : data.trainno, from :data.from, to: data.to}, data, {upsert : true}, cb);
    //};

    SMSProcessedSchema.statics.getAllMessagesD = function(params,cb) {
        this.find(cb);
    };
    SMSProcessedSchema.statics.removeAllMessagesForUser = function(params,cb) {
        this.remove({userId : params.userId, type : 'Transaction'},cb);
    };

    SMSProcessedSchema.statics.getAllMessagesForUserD = function(params,cb) {
        this.find({userId : params.userId}).lean().exec(cb);
    };

    SMSProcessedSchema.statics.getAllTransactionsMessagesForUserD = function(params,cb) {
        this.find({userId : params.userId, type : 'Transaction'}).lean().exec(cb);
    };

    return SMSProcessedSchema;
};
