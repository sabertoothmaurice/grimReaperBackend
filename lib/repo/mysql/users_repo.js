/**
 * Created by maurice on 01/07/15.
 */

var entityModels = require('./entity_models.js');

var UsersRepoD = {
    entity : entityModels.UsersRepo,
    getAllUsersD : function(params, cb) {
        this.entity.findAll({}).complete(cb);
    },
    getUserD : function(params, cb) {
        this.entity.find({where:{userId : params.userId}}).complete(cb);
    },
    getUserMobileD : function(params, cb) {
        this.entity.find({where:{mobileNumber : params.mobileNumber}}).complete(cb);
    },
    getLoginCheckD : function(params, cb) {
        //TODO : add check for password too
        this.entity.find({where:{mobileNumber : params.mobileNumber}}).complete(cb);
    },
    addUserD : function(params, cb) {
        var userId = params.userId;
        var username = params.username;
        var mobileNumber = params.mobileNumber;
        var password = params.password;
        this.entity.create({
            userId      :   userId,
            username    :   username,
            mobileNumber     :   mobileNumber,
            password        :password
        }).complete(cb);
    },
    updateUserD : function(params, cb) {
        var userId = params.userId;
        var username = params.username;
        var picUrl = params.picUrl;
        this.entity.find({ where : {userId : params.userId }}).complete(function(err,row) {
            if(err) {cb(err); return;}
            if (row) {
                row.updateAttributes({userId : userId,username:username,picUrl:picUrl}).complete(cb);
            }else{
                cb("User does not exist");
            }
        });
    },
    updateLastPingTimeD: function(params, cb) {
        var lastPingTime = params.lastPingTime;
        this.entity.find({ where : {userId : params.userId }}).complete(function(err,row) {
            if(err) {cb(err); return;}
            if (row) {
                row.updateAttributes({lastPingTime : lastPingTime}).complete(cb);
            }else{
                cb("User does not exist");
            }
        });
    }

};

module.exports = UsersRepoD;