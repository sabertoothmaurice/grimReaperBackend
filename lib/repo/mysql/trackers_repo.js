/**
 * Created by maurice on 01/07/15.
 */

var entityModels = require('./entity_models.js');

var MessagesRepoD = {
    entity : entityModels.TrackersRepo,
    getAllMessagesD : function(params, cb) {
        var tagsList = params.tags;
        this.entity.findAll({}).complete(cb);
    },
    getHistoryAfterD : function(params, cb) {
        var startTime = params.startTime;
        this.entity.findAll({
            where: {
                timestamp: { gt: startTime}
            }
        }).complete(cb);
    },
    getAllCurrLocationD : function(params, cb) {
        this.entity.findAll({
            group: 'userId DESC'
        }).complete(cb);
    },
    addTrackPointD : function(params, cb) {
        var lat = params.lat;
        var long = params.long;
        var userId = params.userId;
        var timestamp = params.timestamp;
        this.entity.create({
            lat       :   lat,
            long      :   long,
            userId    :   userId,
            timestamp :   timestamp
        }).complete(cb);
    }
};

module.exports = MessagesRepoD;