var Sequelize = require('sequelize');

var TrackersRepo = sequelize.define('TrackersRepo', {
    id : { type : Sequelize.INTEGER, autoIncrement: true, primaryKey: true},
    userId : Sequelize.STRING,
    lat : Sequelize.FLOAT,
    long : Sequelize.FLOAT,
    timestamp : Sequelize.BIGINT
}, {
    freezeTableName: true,
    tableName: 'trackers',
    timestamps: false
});

var UsersRepo = sequelize.define('UsersRepo', {
    id : { type : Sequelize.INTEGER, autoIncrement: true },
    userId : { type : Sequelize.STRING, primaryKey: true , unique : true},
    username : Sequelize.STRING,
    password : Sequelize.STRING,
    mobileNumber : { type : Sequelize.STRING, unique : true}
}, {
    freezeTableName: true,
    tableName: 'users',
    timestamps: true
});


exports.TrackersRepo = TrackersRepo;
exports.UsersRepo = UsersRepo;


// sequelize
//   .sync({ force: true })
//   .complete(function(err) {
//      if (!!err) {
//        console.log('An error occurred while creating the table:', err)
//      } else {
//        console.log('It worked!')
//      }
//   });