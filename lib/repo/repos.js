/**
 *      Contains all the data repos that we are using (can be either serving from mongoDb or mySQL)
 */

var trackersRepo = require('./mysql/trackers_repo.js');
//var usersRepo = require('./mysql/users_repo.js');
var usersRepo = require('./mongodb_repos.js').Users;
var cardsRepo = require('./mongodb_repos.js').Cards;

var vendorsRepo = require('./mongodb_repos.js').Vendors;
var smsdumpRepo = require('./mongodb_repos.js').SmsDump;
var smsProcessedRepo = require('./mongodb_repos.js').SmsProcessedDump;

var regexRepo = require('./mongodb_repos.js').Regex;
var bucketRepo = require('./mongodb_repos.js').Buckets;
var prebucketRepo = require('./mongodb_repos.js').PreBuckets;

var Repos = {
    trackersRepo        :   trackersRepo,
    usersRepo           :   usersRepo,
    smsdumpRepo         :   smsdumpRepo,
    vendorsRepo         :   vendorsRepo,
    regexRepo           :   regexRepo,
    smsProcessedRepo    :   smsProcessedRepo,
    bucketRepo          :   bucketRepo,
    cardsRepo           :   cardsRepo,
    prebucketRepo       :   prebucketRepo
};

module.exports = Repos;