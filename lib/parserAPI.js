'use strict';
var deferred = require('./common-utils/deferred');
var fn = require('./common-utils/functions');
var repos = require('./repo/repos.js');
var _ = require('underscore');
var uuid = require('node-uuid');
var regexp = require('node-regexp');
var constants = new (require("./common-utils/constants.js"))();


function ParserAPI() {}

function apiRespose(success,result,status){//use this to reply to all API requests
    if(!status) status = 200;
    if(success){
        return deferred.success({
            status		:	'success',
            message	    :	null,
            result		:	result,
            statusCode  :   status
        });
    }else{
        return deferred.success({
            status		:	'error',
            message	    :	result,
            statusCode  :   status
        },status);
    }
}

//CRUD
ParserAPI.prototype.parseForUser = function (params) {
    var startTime = (new Date()).getTime();
    var access_token = params.access_token || params.query.access_token;
    return fn.defer(fn.bind(repos.usersRepo, 'getUserByToken'))({
        access_token        :   access_token
    }).pipe(function(user){

        var deferredCalls = {};
        deferredCalls.smsDump = fn.defer(fn.bind(repos.smsdumpRepo, 'getAllMessagesForUserD'))({userId : user._id});
        deferredCalls.smsProcessed = fn.defer(fn.bind(repos.smsProcessedRepo, 'removeAllMessagesForUser'))({userId : user._id});
        deferredCalls.regexPatterns = fn.defer(fn.bind(repos.regexRepo, 'getAllRegexesD'))({});
        return deferred.combine(deferredCalls).pipe(function(data){

            var smsDump = data.smsDump;
            var regexPatterns = data.regexPatterns;

            var processedSMS = [];

            //Match all Patterns
            smsDump.forEach(function(sms){
                for(var i=0;i<regexPatterns.length;i++){

                    var passed = true;
                    var regexPattern = regexPatterns[i];


                    //Check for must have texts
                    var mustHaveTexts = (regexPattern.regexScan)?((regexPattern.regexScan.must)?regexPattern.regexScan.must:[]):[];
                    mustHaveTexts.forEach(function(text){
                        passed = passed && regexp().find(text).toRegExp().test(sms.body);
                    });

                    if(passed){

                        //Start extraction
                        var regexExtract = regexPattern.regexExtract || [];
                        var values = {};
                        regexExtract.forEach(function(extractObj){
                            var regex = new RegExp(extractObj.valueRegex);
                            var regexMatch  = regex.exec(sms.body);
                            if(regexMatch){
                                values[extractObj.valueName] = parseInt(regexMatch[1]);
                            }else{
                                console.logger.error("Regex pattern not found in Regex of ID : "+regexPattern._id);
                            }
                        });


                        processedSMS.push({
                            regexId : regexPattern._id,
                            body : sms.body,
                            time : sms.time,
                            userId : sms.userId,
                            from : sms.from,
                            data  : values,
                            type : regexPattern.resultType,
                            vendorId : regexPattern.vendorId,
                            uniqueId : sms.uniqueId
                        });

                        break;
                    }
                }
            });

            //Save these Processed Messages in separate DB
            var deferredCalls = {};
            processedSMS.forEach(function(msg){
                deferredCalls[msg.uniqueId] = fn.defer(fn.bind(repos.smsProcessedRepo, 'updateSMSProcessedD'))(msg);
            });
            return deferred.combine(deferredCalls).pipe(function(result){
                var inserted = 0;
                var updated = 0;
                for(var key in result){
                    if(result[key].upserted) inserted++;
                    else updated++;
                }
                console.log(inserted+" messages Inserted, "+updated+" messages updated");

                console.log("Parsed SMSs for User : "+user._id+" in "+((new Date()).getTime()-startTime)+" msecs")
                return apiRespose(true,{user:user,regexPatterns:regexPatterns},200);
            });



        });

//
//
//        //filter only those points that are far apart
//        var upsertArr = [];
//        smsdump.forEach(function(sms){
//            upsertArr.push({
//                id : sms.id,
//                from : sms.number,
//                time : sms.date,
//                body : sms.body,
//                userId : user._id,
//                raw : JSON.stringify(sms),
//                uniqueId : sms.id
//
//            });
//        });
//        var deferredCalls = {};
//        upsertArr.forEach(function(msg){
//            deferredCalls[msg.uniqueId] = fn.defer(fn.bind(repos.smsdumpRepo, 'updateSMS'))(msg);
//        });
//        return deferred.combine(deferredCalls).pipe(function(result){
//            var inserted = 0;
//            var updated = 0;
//            for(var key in result){
//                if(result[key].upserted) inserted++;
//                else updated++;
//            }
//            console.log(inserted+" messages Inserted, "+updated+" messages updated");
//            return apiRespose(true,{user:user},200);
//        });

    });
};


ParserAPI.prototype.regerateBuckets = function (params) {
    var deferredCallsL1 = {};
    var self = this;
    deferredCallsL1.buckets = fn.defer(fn.bind(repos.bucketRepo, 'getAllBucketsD'))({});
    deferredCallsL1.smsRaw = fn.defer(fn.bind(repos.smsdumpRepo, 'getRandomMessages'))({});
    return deferred.combine(deferredCallsL1).pipe(function(dataL1) {
        var buckets = dataL1.buckets;
        var smsRawWithoutBuckets = getMessagesThatAreNotInAnyBucket(dataL1.smsRaw,buckets);
        console.log("found "+smsRawWithoutBuckets.length+" random sms without buckets");

        //Find 1000 random messages from it

        return self.regerateBucketsForMsgs(smsRawWithoutBuckets,5).pipe(function(dataL2){
            return apiRespose(true,{buckets : dataL2},200);
        });
    });
};

ParserAPI.prototype.regerateBucketsFromUserMsg = function (params) {
    console.log("Started regerateBucketsFromUserMsg");
    var userId = params.userId;
    var deferredCallsL1 = {};
    var self = this;
    deferredCallsL1.buckets = fn.defer(fn.bind(repos.bucketRepo, 'getAllBucketsD'))({});
    deferredCallsL1.smsRaw = fn.defer(fn.bind(repos.smsdumpRepo, 'getRandomMessages'))({limit : 1000});
    return deferred.combine(deferredCallsL1).pipe(function(dataL1) {
        var buckets = dataL1.buckets;
        console.log("found "+dataL1.smsRaw.length+" random sms");
        var smsRawWithoutBuckets = getMessagesThatAreNotInAnyBucket(dataL1.smsRaw,buckets);
        console.log("found "+smsRawWithoutBuckets.length+" sms without buckets");
        return self.regerateBucketsForMsgs(smsRawWithoutBuckets,5).pipe(function(dataL2){
            return apiRespose(true,{buckets : dataL2},200);
        });
    });
};

ParserAPI.prototype.regerateBucketsForMsgs = function (smsRawArr, bucketSizeLimit) {

    console.log("Started generating Buckets form "+smsRawArr.length+" messages");
    //Order into Buckets
    var buckets = orderIntoBuckets(smsRawArr);
    console.log("orderIntoBuckets : ",buckets);


    //Find Variables in buckets
    console.logger.info("Finding variables for buckets....");
    var bucketsValid = [];
    buckets.forEach(function(bucket){
        if(bucket.msgs.length<bucketSizeLimit) return;//ignore this
        console.log("bucket.msgs.length",bucket.msgs.length);
//        console.log("bucket.msgs",bucket.msgs);

        var diffs = [];
        bucket.msgs.splice(1).forEach(function(msg){
            var diffArr = findDiffBetweenString(bucket.msgs[0].body,msg.body);
            diffArr.forEach(function(diff){
                var startIndex = bucket.msgs[0].body.indexOf(diff);
                var endIndex = startIndex+diff.length;
                diffs.push({
                    startIndex  :   startIndex,
                    endIndex    :   endIndex,
                    diff        :   diff
                })
            });
        });
        diffs = clubRepeatedDiffs(diffs,bucket.msgs[0].body);

        bucket.diffArr = _.pluck(diffs, 'diff');
        bucket.diffs = diffs;
        bucket.diffSample = bucket.msgs[0].body;
        var sampleSet = [];
        bucket.msgs.slice(0,5).forEach(function(msg){sampleSet.push(msg.body)});
        bucket.sampleBodySet = sampleSet;
        bucketsValid.push(bucket);
    });


    //Generate Regex for the variable
    var uniqueCode = "LKSFADGDSG";//some unique code to continue with replace algorithm
    bucketsValid.forEach(function(bucket){
        var sample = bucket.diffSample;
        bucket.diffArr.forEach(function(variable){
            sample = sample.split(variable).join(uniqueCode);
        });
        sample = escapeRegExp(sample);
        bucket.regex = sample.split(uniqueCode).join("(.+)");
//        console.log("REGEX : "+bucket.regex);
    });

    //Test Regex values
    var bucketsPassed = [];
    bucketsValid.forEach(function(bucket){
        var regex = new RegExp(bucket.regex);
        var regexMatch  = regex.exec(bucket.diffSample);

        if(regexMatch==null){
            //TODO : some issue, reject this
            console.logger.error("Rejected a Bucket for Unknown Reason : ",bucket);
            return;
        }

        bucket.testVariablesExtracted = regexMatch.slice(1);

        //TODO : remove an infinite loop issue
        if(bucket.regex.split("(.+)").length>10) return;//reject ones with more than 10 variables
        //the following is the part of the body where it is going infinite
        //g & Get loan amount Credited in 1 hour* + 500 Bonus Reward Points (Visit www.hdfcbank.com/instatc for details). To know eligible loan amount, EMI & apply, Login to NetBanking & Click Cards > Credit Card

        //var check if regex has minimum characters : (eliminate '(.+)')
        var regex2 = bucket.regex+"";
        regex2 = regex2.split("(.+)").join("");
        if(regex2.split(" ").length<5) return;//min 5 words
        //TODO : make it accept smaller messages too like "your OTP is XXXXXX"
        console.log("Passed : "+regex2 + " : ",+regex2.split(" "));
        bucketsPassed.push(bucket);
    });

    //Form new buckets
    var bucketsNew = [];
    bucketsPassed.forEach(function(bucket){
        var variables = [];
        var count = 1;
        bucket.diffs.forEach(function(){variables.push("variable"+(count++))})
        if(bucket.regex == ("(.))")) return;
        bucketsNew.push({
            name : "Unknown Bucket",
            from        : bucket.sample.from,
            vendorId    :   null,
            regex :         bucket.regex,
            variables    :   variables,
            sample    :   bucket.sampleBodySet
        });
    });

    var deferredCallsL1 = {};
    var count = 0;
    if(bucketsNew.length==0) return deferred.success([]);

    return apiRespose(true,{bucketsNew:bucketsNew});

    bucketsNew.forEach(function(bucket){
        deferredCallsL1[count++] = fn.defer(fn.bind(repos.bucketRepo, 'createBucketD'))(bucket);
    });
    return deferred.combine(deferredCallsL1).pipe(function(result){
        return deferred.success(bucketsPassed);
    });

};


function getMessagesThatAreNotInAnyBucket(smsRawArr,buckets){
    var regexArr = [];
    var smsWithoutBuckets = [];
    buckets.forEach(function(bucket){
        regexArr.push(new RegExp(bucket.regex));
    });

    //Find messages that don't match any Regex Expression
    smsRawArr.forEach(function(smsRaw){
        var match = false;
        regexArr.forEach(function(regex){
            if(match) return;
            if(regex.exec(smsRaw.body)) match = true;
        });
        if(!match) smsWithoutBuckets.push(smsRaw);
    });

    return smsWithoutBuckets;

}

function findDiffBetweenString(str1,str2,arrSplit){ //finds all strings that are different in str2 that are in str1
    arrSplit = [" ",",",".","(",")"];

    var str1New = ""+str1;
    arrSplit.forEach(function(token){str1New = str1New.split(token).join(" ")});
    var str1Arr = str1New.split(" ");

    var str2New = ""+str2;
    arrSplit.forEach(function(token){str2New = str2New.split(token).join(" ")});
    var str2Arr = str2New.split(" ");

    var diff = [];
    str1Arr.forEach(function(str){
        if(str2Arr.indexOf(str)<0) diff.push(str);
    });

//    console.log("diff : ",diff);
//    console.log("str1 : ",str1);
//    console.log("str2 : ",str2);
//    console.log("str1New : ",str1New);

    //club space seperated diffs
    var previousStart = 0;
    var previousEnd = 0;
    var diffFinal = [];
    diff.forEach(function(diffStr){
        var startIndex = str1.indexOf(diffStr);
        var endIndex = startIndex+diffStr.length;
        var betweenText = str1.substr(previousEnd, startIndex-previousEnd);
        arrSplit.forEach(function(token){betweenText = betweenText.split(token).join("")});
//        console.log("betweenText : ",betweenText);
        if(betweenText.length!=0){
//            console.log("Pushing : "+previousStart+" : "+previousEnd,str1.substr(previousStart,previousEnd-previousStart));
            diffFinal.push(str1.substr(previousStart,previousEnd-previousStart));
            previousStart = startIndex;
            previousEnd = endIndex;
        }else{
            previousEnd = endIndex;
        }
    });
    diffFinal.push(str1.substr(previousStart,previousEnd-previousStart));//last one too

    while(diffFinal.indexOf("")>-1){
        var index = diffFinal.indexOf("");
        diffFinal.splice(index, 1);
    }


//    console.log("diffFinal : ",diffFinal);
//    console.log("-------------");

    return diffFinal;
}

function orderIntoBuckets(msgsArr){
    var count = 0;
    console.log("Started orderIntoBuckets form "+msgsArr.length+" messages");
    var buckets = [];
    msgsArr.forEach(function(sms){
        var found = false;
//        console.log("Checking sms "+(count++)+" among "+buckets.length+" buckets : "+sms.body);

        buckets.forEach(function(bucket){
//            console.log("REACH1 : "+bucket.sample.body);
            if(!found&&isMessagesSimilar(bucket.sample.body,sms.body)){
                console.log("FOUNd Match : "+bucket.sample.body+" : "+sms.body);
                if(bucket.msgs.length<5) bucket.msgs.push(sms);
                bucket.length++;
                found = true;
            }
        });

        if(!found){
            buckets.push({
                sample : sms,
                msgs : [sms],
                length : 1
            });

            //Get only 50 top buckets
            if(buckets.length>200){
                console.log("Trimming to only "+buckets.length+" buckets");
                buckets.sort(function(b1, b2){return b2.length-b1.length});
                buckets.splice(-1);
            }
        }
    });
    console.log("finished orderIntoBuckets into: "+buckets.length);
    return buckets;
}

function isMessagesSimilar(str1, str2){
//    console.log("Checking similarity : "+str1+" : "+str2);
    var str1Arr = str1.split(" ");
    var str2Arr = str2.split(" ");
    if(str1Arr.length<5||str2Arr.length<5) return false;
    var minMatchPercent = 0.3;
    var match = 0, unmatch = 0;
    str1Arr.forEach(function(str){
        if(str2Arr.indexOf(str)>=0) match++;
        else unmatch++;
    });
//    console.log("Match : "+match);
//    console.log("unmatch : "+unmatch);
    return unmatch / (match + unmatch) <= minMatchPercent;



}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}
function unescapeRegExp(str) {
    return str.replace(/\\\-/g,"-").replace(/\\\[/g,"[").replace(/\\\]/g,"]").replace(/\\\//g,"/").replace(/\\\{/g,"{")
        .replace(/\\\}/g,"}").replace(/\\\(/g,"(").replace(/\\\)/g,")").replace(/\\\*/g,"*").replace(/\\\+/g,"+").replace(/\\\?/g,"?")
        .replace(/\\\./g,".").replace(/\\\\/g,"\\").replace(/\\\^/g,"^").replace(/\\\$/g,"$").replace(/\\\|/g,"|");
}
function clubRepeatedDiffs(diffs,body){
    console.log("Clubbing repeated : "+body+"-----------------------");
    var newDiffs = [];
    var arr = {};
    diffs.forEach(function(diff){
        for(var i=diff.startIndex;i<diff.endIndex;i++){
            arr[i] = 1;
        }
    });
    arr[body.length] = null;
    var prev = null;
    var starts = [];
    var ends = [];
    for(var i=0;i<body.length+1;i++){
        if((arr[i]==null||i==0)&&(arr[i+1]!=null)){
            starts.push((i==0)?i:i+1);
        }
        if((arr[i+1]==null)&&(arr[i]!=null)){
            ends.push(i+1);
        }
    }
    for(var i=0;i<starts.length;i++){
        newDiffs.push({
            startIndex  :   starts[i],
            endIndex    :   ends[i],
            diff        :   body.substr(starts[i],ends[i]-starts[i])
        });
    }
//    console.log("diffs",diffs);
//    console.log("newDiffs",newDiffs);
    return newDiffs;
}



ParserAPI.prototype.sanitizeBuckets = function (params) {
    var deferredCallsL1 = {};
    var self = this;
    deferredCallsL1.buckets = fn.defer(fn.bind(repos.bucketRepo, 'getAllBucketsD'))({});
    return deferred.combine(deferredCallsL1).pipe(function(dataL1) {
        var buckets = dataL1.buckets;
        console.log("found "+buckets.length+" buckets");


        //TODO : generate smsArr so that we can directly pass this into original funcitons built
        var smsArr = [];
        buckets.forEach(function(bucket){
            smsArr.push({
                body : unescapeRegExp(bucket.regex.split("(.+)").join(getUniqueCode()))
            })
        });

        //TODO : Generate generic buckets
        console.log("regerating buckets from "+smsArr.length+" sms");
        return self.regerateBucketsForMsgs(smsArr,2).pipe(function(dataL2){

            //TODO : Remove child buckets
            //Find child buckets
            var out = [];
            var child = {};
            var filteredBuckets = [];
            var removedBuckets = [];
            var removedBucketIds = [];

            buckets.forEach(function(bucket1){
                buckets.forEach(function(bucket2){
                    var regex = new RegExp(bucket1.regex);
                    var regexMatch  = regex.exec(unescapeRegExp(bucket2.regex.split("(.+)").join(getUniqueCode())));
                    if(regexMatch!=null){
                        if(bucket1._id!=bucket2._id){
                            removedBuckets.push(bucket2._id);
                            removedBucketIds.push(bucket2._id);
                            child[bucket1.regex] = bucket2.regex;
                            out.push(bucket1.regex);
                            out.push(bucket2.regex);
                        }
                    }
                });
            });

            console.log("found "+removedBuckets.length+" buckets that are children");
            var deferredCallsL3 = {};
            deferredCallsL3.removeBuckets = fn.defer(fn.bind(repos.bucketRepo, 'removeBucketByIds'))({ids:removedBucketIds});
            return deferred.combine(deferredCallsL3).pipe(function(dataL3) {
                console.log("removed " + removedBuckets.length + " child buckets");
                return apiRespose(true,{out : dataL2},200);
            });
        });






//        var ordered = orderTextIntoBuckets(out);



//        buckets.sort(function(a,b){
//            var A = a.regex;
//            var B = b.regex;
//            if (A < B){
//                return -1;
//            }else if (A > B){
//                return  1;
//            }else{
//                return 0;
//            }
//        });

//        buckets.forEach(function(bucket1){bucket1.matches = 0;});
//
//        buckets.forEach(function(bucket1){
//
//            buckets.forEach(function(bucket2){
//                var regex = new RegExp(bucket1.regex);
//                console.log("REGEX : "+bucket1.regex+" : "+bucket2.regex.split("(.+)").join("XX"));
//                var regexMatch  = regex.exec(bucket2.regex.split("(.+)").join("XX"));
//                if(regexMatch){
//                    bucket1.matches++;
//                }
//            });
//        });
//        //Find 1000 random messages from it
//
//        buckets.sort(function(a,b){b.matches - a.matches});


//        return self.regerateBucketsForMsgs(smsRawWithoutBuckets).pipe(function(dataL2){
//        });
    });
};
function orderTextIntoBuckets(txtsArr){
    var count = 0;
    console.log("Started orderIntoBuckets form "+txtsArr.length+" messages");
    var buckets = [];
    txtsArr.forEach(function(txt){
        var found = false;
//        console.log("Checking sms "+(count++)+" among "+buckets.length+" buckets : "+sms.body);

        buckets.forEach(function(bucket){
            if(!found&&isMessagesSimilar(bucket.sample,txt)){
                if(bucket.msgs.length<5) bucket.msgs.push(txt);
                bucket.length++;
                found = true;
            }
        });

        if(!found){
            buckets.push({
                sample : txt,
                msgs : [txt],
                length : 1
            });
        }
    });
    console.log("finished orderIntoBuckets");
    return buckets;
}

function findRegexSubSets(arr){

}

function getUniqueCode(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for( var i=0; i < 4; i++ ) text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}




module.exports = ParserAPI;
