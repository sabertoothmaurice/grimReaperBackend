'use strict';

var moment = require('moment');

//Constant strings


function Constants(){
    this.type_transaction = 'Transaction';
    this.type_miscellaneous = 'Miscellaneous';

    this.categoryTypes = [this.type_miscellaneous,this.type_transaction];
    this.categoryDefaultType = this.type_miscellaneous;

    //Buckets Categories
    this.bucket_cat_otp = 'otp';
    this.bucket_cat_spam = 'spam';
    this.bucket_cat_unknown = 'unknown';
    this.bucket_cat_transaction = 'transaction';
    this.bucket_cat_orderstatus = 'orderstatus';

    this.bucket_cat = [
        this.bucket_cat_otp,
        this.bucket_cat_spam,
        this.bucket_cat_unknown,
        this.bucket_cat_transaction,
        this.bucket_cat_orderstatus]

    //Buckets Variable Types
    this.variable_code_cost = 'cost';
    this.variable_code_date = 'date';
    this.variable_code_unknown = 'unknown';
    this.variable_code_username = 'username';
    this.variable_code_name = 'name';
    this.variable_code_phone = 'phone';
    this.variable_code_time = 'time';
    this.variable_code_id = 'id';
    this.variable_code_otp = 'otp';
    this.variable_code_earn = 'earn';
    this.variable_code_place = 'place';
    this.variable_code_deliveredBy = 'deliveredBy';

    this.variable_code = [
        this.variable_code_name,
        this.variable_code_phone,
        this.variable_code_cost,
        this.variable_code_username,
        this.variable_code_date,
        this.variable_code_unknown,
        this.variable_code_time,
        this.variable_code_otp,
        this.variable_code_earn,
        this.variable_code_place,
        this.variable_code_deliveredBy,
        this.variable_code_id]

}

module.exports = Constants;