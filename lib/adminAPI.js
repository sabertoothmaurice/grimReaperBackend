'use strict';
var deferred = require('./common-utils/deferred');
var fn = require('./common-utils/functions');
var repos = require('./repo/repos.js');
var underscore = require('underscore');
var uuid = require('node-uuid');


function DataAPI() {}

function apiRespose(success,result,status){//use this to reply to all API requests
    if(!status) status = 200;
    if(success){
        return deferred.success({
            status		:	'success',
            message	    :	null,
            result		:	result,
            statusCode  :   status
        });
    }else{
        return deferred.success({
            status		:	'error',
            message	    :	result,
            statusCode  :   status
        },status);
    }
}

//CRUD
DataAPI.prototype.getAllUsers = function (params) {
    return fn.defer(fn.bind(repos.usersRepo, 'getAllUsersD'))({}).pipe(function(data){
        return apiRespose(true,{users:data},200);
    });
};

DataAPI.prototype.getAllMessages = function (params) {
    return fn.defer(fn.bind(repos.smsdumpRepo, 'getAllMessagesD'))({}).pipe(function(data){
        return apiRespose(true,{data:data},200);
    });
};

DataAPI.prototype.getAllMessagesForEmail = function (params) {
    var email = params.email;
    return fn.defer(fn.bind(repos.usersRepo, 'getUserByEmail'))({email : email}).pipe(function(user){
        if(!user) return apiRespose(false,"No registered user with email : "+email);
        var userId = user._id;
        return fn.defer(fn.bind(repos.smsdumpRepo, 'getAllMessagesForUserD'))({userId:userId}).pipe(function(data){
            return apiRespose(true,{data:data},200);
        });
    });
};





module.exports = DataAPI;
