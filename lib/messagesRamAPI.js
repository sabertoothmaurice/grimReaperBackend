'use strict';
var deferred = require('./common-utils/deferred');
var fn = require('./common-utils/functions');
var repos = require('./repo/repos.js');
var _ = require('underscore');
var uuid = require('node-uuid');
var activeThreads = [];
activeThreads.push({
    threadId : 'Global',
    name    :   'Global Group',
    description : 'A room with all TinyStep Users',
    messages : []
});
activeThreads.push({
    threadId : 'Local',
    name    :   'Local Group',
    description : 'A room with some TinyStep Users',
    messages : []
});
var users = [];
var userOnlineStatus = [];
var inactiveTime = 10*1000;//10 seconds
var messagesCache = 1000;//store 1000 previous messages in ram (actually we need to store messages in last 1 minute)



function MessagesRamAPI() {}

function apiRespose(success,result){//use this to reply to all API requests
    if(success){
        return deferred.success({
            status		:	'success',
            message	    :	null,
            result		:	result
        });
    }else{
        return deferred.success({
            status		:	'error',
            message	    :	result,
            result		:	null
        });
    }
}

function getThreadOfId(threadId){
    for(var index in activeThreads){
        if(activeThreads[index].threadId==threadId){
            return activeThreads[index];
        }
    }
    return null;
}
function getOnlineUsersInGroup(groupId){
    if(!userOnlineStatus[groupId]) return 0;
    var count =0;
    for(var userId in userOnlineStatus[groupId]){
        if((new Date()).getTime()-userOnlineStatus[groupId][userId]<inactiveTime) count++;
    }
    return count;
}
function checkInOnline(userId,groupId){
    if(!userOnlineStatus[groupId]) userOnlineStatus[groupId] = [];
    userOnlineStatus[groupId][userId] =  (new Date()).getTime();
}

//CRUD
MessagesRamAPI.prototype.readMessagesRaw = function (params) {
    var userId = params.userId;
    var prevMsgTime = params.prevMsgTime;
    var threadId = params.threadId;

    var thread = getThreadOfId(threadId);
    if(!thread) return apiRespose(true,{msgs:[]});
    var allMsgs = thread.messages;
    checkInOnline(userId,threadId);
    var sendMsgs = [];
    for(var index in allMsgs){
        var msg = allMsgs[allMsgs.length-1-index];
        if(msg.timestamp>prevMsgTime)sendMsgs.push({
            message: msg.message,
            from: msg.from,
            threadId: msg.threadId,
            timestamp: msg.timestamp
        });
    }

    return apiRespose(true,{
        msgs    :   sendMsgs,
        online  :   getOnlineUsersInGroup(threadId)});
};
MessagesRamAPI.prototype.addMessageRaw = function (params) {
    console.log('params',params);
    var userId = params.userId;
    var message = params.post.message;
    var threadId = params.post.threadId;
    var picUrl = params.post.picUrl;
    var username = params.post.username;
    checkInOnline(userId,threadId);
    var thread = getThreadOfId(threadId);
    if(!thread) apiRespose(true,{data:null});

    var data = {
        message     :   message,
        from        :   {
            userId : userId,
            username : username,
            picUrl : picUrl

        },
        threadId    :   threadId,
        timestamp   :   (new Date()).getTime()
    };
    if(thread.messages.length>messagesCache) thread.messages.splice(0,1);
    thread.messages.push(data);
    return apiRespose(true,{data:data});

};
MessagesRamAPI.prototype.registerAndGetIrcGroupsRaw = function (params) {
    var userId = params.userId;
    var username = params.post.username;
    var picUrl = params.post.picUrl;

    users[userId] = {
        userId:userId,
        username:username,
        picUrl:picUrl
    };

    var threadList = [];
    for(var index in activeThreads){
        var thread = activeThreads[index];
        threadList.push({
            threadId    :   thread.threadId,
            name        :   thread.name,
            description :   thread.description,
            online      :   getOnlineUsersInGroup(thread.threadId)
        });
    }

    return apiRespose(true,{ircRooms : threadList});

};
MessagesRamAPI.prototype.registerUserRaw = function (params) {
    console.log('registerUserRaw params',params);
    var userId = params.userId;
    var username = params.post.username;
    var picUrl = params.post.picUrl;
    users[userId] = {
        userId:userId,
        username:username,
        picUrl:picUrl
    };
    return apiRespose(true,{data:data});

};

module.exports = MessagesRamAPI;
