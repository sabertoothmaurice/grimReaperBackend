'use strict';
var deferred = require('./common-utils/deferred');
var fn = require('./common-utils/functions');
var repos = require('./repo/repos.js');
var underscore = require('underscore');
var uuid = require('node-uuid');


function UsersAPI() {}

function apiRespose(success,result,status){//use this to reply to all API requests
    if(!status) status = 200;
    if(success){
        return deferred.success({
            status		:	'success',
            message	    :	null,
            result		:	result,
            statusCode  :   status
        });
    }else{
        return deferred.success({
            status		:	'error',
            message	    :	result,
            statusCode  :   status
        },status);
    }
}

//CRUD
UsersAPI.prototype.registerUser = function (params) {
    console.log('params',params);
    var userId = uuid.v4();
    var mobile = params.mobile || params.post.mobile;
    var username = params.name || params.post.name;
    var password = params.password || params.post.password;
    return fn.defer(fn.bind(repos.usersRepo, 'getUserMobileD'))({
        mobileNumber     :   mobile
    }).pipe(function(existingUser){
        if(existingUser){
            return apiRespose(false,"Duplicate User",409);
        }else{
            return fn.defer(fn.bind(repos.usersRepo, 'addUserD'))({
                userId      :   userId,
                username    :   username,
                mobileNumber:   mobile,
                password    :   password
            }).pipe(function(data) {
                return apiRespose(true,{user:data},200);
            });
        }
    });
};
UsersAPI.prototype.loginUser = function (params) {
    console.log('params',params);
    var mobile = params.mobile || params.post.mobile;
    var password = params.password || params.post.password;
    return fn.defer(fn.bind(repos.usersRepo, 'getLoginCheckD'))({
        mobileNumber        :   mobile,
        password            :   password
    }).pipe(function(existingUser){
        if(!existingUser){
            return apiRespose(false,"User does not exist");
        }else{
            return apiRespose(true,{ user : existingUser});
        }
    });
};
UsersAPI.prototype.loginGoogle = function (params) {
    console.log('params',params);
    var email = params.email || params.post.email;
    var access_token = params.access_token || params.post.access_token;//TODO : implement this
    return fn.defer(fn.bind(repos.usersRepo, 'getUserByEmail'))({
        email        :   email
    }).pipe(function(existingUser){
        if(!existingUser){
            console.log("New user Detected");
            var access_token = uuid.v4();
            return fn.defer(fn.bind(repos.usersRepo, 'createUserByEmail'))({
                email       :   email,
                access_token : access_token
            }).pipe(function(data) {
                return apiRespose(true,{
                    user : {
                        email : data.email,
                        access_token : data.access_token,
                        userId : data._id
                    }
                },200);
            });

        }else{
            return apiRespose(true,{
                user : {
                    email : existingUser.email,
                    access_token : existingUser.access_token,
                    userId : existingUser._id
                }
            });
        }
    });
};

UsersAPI.prototype.addTrackPoint = function (params) {
    console.log('params',params);
    var userId = params.userId || params.post.userId;
    var lat = params.lat || params.post.lat;
    var long = params.long || params.post.long;
    if(!long || !lat) return apiRespose(false,"Lat Long not specified");
    var timestamp = params.timestamp || params.post.timestamp;
    return fn.defer(fn.bind(repos.usersRepo, 'getUserD'))({
        userId        :   userId
    }).pipe(function(existingUser){
        if(!existingUser){
            return apiRespose(false,"User does not exist");
        }else{
            return fn.defer(fn.bind(repos.trackersRepo, 'addTrackPointD'))({
                lat       :   lat,
                long      :   long,
                userId    :   userId,
                timestamp :   timestamp
            }).pipe(function(existingUser){

                return apiRespose(true,{ user : existingUser});
            });
        }
    });
};




module.exports = UsersAPI;
