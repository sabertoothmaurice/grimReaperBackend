Main Node Server to serve API requests from Android App

to setup the environment after cloning this project do the following :

*) download & install node - https://nodejs.org/download/
*) download & install mongodb - https://www.mongodb.org/downloads
*) clone repo into wehive directory - 'git clone git@gitlab.com:wemake/node-irc.git'
*) run shell script - 'sudo ./setup'
*) run server - 'nodemon app'
*) check in browser - http://localhost:4000/threads

for more details : contact Maurice(sabertoothmaurice@gmail.com)
